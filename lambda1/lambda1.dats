(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload "./../mylib/mylib.sats"

(* ****** ****** *)

typedef vnam = string
typedef opnm = string

(* ****** ****** *)

datatype term = 
//
  | TMint of int
  | TMstr of string
//
  | TMvar of vnam
  | TMlam of (vnam, term)
  | TMapp of (term, term)
//
  | TMifz of (term, term, term)
//
  | TMfix of (vnam, vnam, term) // Y(lam f.lam x.<body>)
//
  | TMopr of (opnm, termlst)
//
  | TMtup of (termlst) // tuple construction
  | TMprj of (term, int) // tuple projection
//
  | TMlet of (vnam, term, term) // let x = t1 in t2 end
//
where termlst = list0(term)

(* ****** ****** *)

extern
fun
print_term(term): void // stdout
and
prerr_term(term): void // stderr
and
fprint_term(FILEref, term): void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(tm) =
fprint_term(stdout_ref, tm)
implement
prerr_term(tm) =
fprint_term(stderr_ref, tm)

(* ****** ****** *)

implement
fprint_val<term> = fprint_term

(* ****** ****** *)

implement
fprint_term(out, tm0) =
(
case+ tm0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMstr(x) =>
  fprint!(out, "TMstr(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tm1) =>
  fprint!(out, "TMlam(", x, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, x, tm1) =>
  fprint!(out, "TMfix(", f, "; ", x, "; ", tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
| TMtup(tms) =>
  fprint!(out, "TMtup(", tms, ")")
| TMprj(tm1, idx) =>
  fprint!(out, "TMprj(", tm1, "; ", idx, ")")
| TMlet(x0, tm1, tm2) =>
  fprint!(out, "TMlet(", x0, "; ", tm1, "; ", tm2, ")")
)

(* ****** ****** *)

datatype
value =
| VALint of int
| VALstr of string
| VALtup of values
| VALlam of (term, envir)
| VALfix of (term, envir)

where
values = list0(value)
and
envir = list0($tup(vnam, value))

(* ****** ****** *)

extern
fun
print_value(value): void // stdout
and
prerr_value(value): void // stderr
and
fprint_value(FILEref, value): void

overload print with print_value
overload prerr with prerr_value
overload fprint with fprint_value

(* ****** ****** *)

implement
print_value(x0) =
fprint_value(stdout_ref, x0)
implement
prerr_value(x0) =
fprint_value(stderr_ref, x0)

(* ****** ****** *)

implement
fprint_val<value> = fprint_value

(* ****** ****** *)

implement
fprint_value(out, vl0) =
(
case+ vl0 of
| VALint(x) =>
  fprint!(out, "VALint(", x, ")")
| VALstr(x) =>
  fprint!(out, "VALstr(", x, ")")
| VALtup(xs) =>
  fprint!(out, "VALtup(", xs, ")")
| VALlam(tm0, env) =>
  fprint!(out, "VALlam(", tm0, "; ", "...", ")")
| VALfix(tm0, env) =>
  fprint!(out, "VALfix(", tm0, "; ", "...", ")")
)

(* ****** ****** *)

extern
fun
interp0 : term -> value
extern
fun
interp1 : (term, envir) -> value

extern
fun
interp1_var : (term, envir) -> value
extern
fun
interp1_tup : (term, envir) -> value
extern
fun
interp1_app : (term, envir) -> value

(* ****** ****** *)

implement
interp0(t0) =
interp1(t0, list0_nil())

implement
interp1(t0, env) =
(
case- t0 of
| TMint(i) => VALint(i)
| TMstr(s) => VALstr(s)
| TMvar(x) =>
  interp1_var(t0, env)
| TMtup(ts) =>
  interp1_tup(t0, env)
//
| TMlam(x, t) => VALlam(t0, env)
//
| TMapp(_, _) =>
  interp1_app(t0, env)
//
| TMifz(t1, t2, t3) =>
  let
    val v1 = interp1(t1, env)
  in
    case- v1 of
    | VALint(i1) =>
      if i1 = 0
      then interp1(t2, env) else interp1(t3, env)
  end
//
| TMfix(f, x, t) => VALfix(t0, env)
//
| TMlet(x0, t1, t2) =>
  let
    val v1 = interp1(t1, env)
  in
    interp1(t2, env1) where
    {
      val env1 =
      list0_cons($tup(x0, v1), env)
    }
  end
)

(* ****** ****** *)

implement
interp1_var
  (t0, env) =
let
val-TMvar(x0) = t0
in
(
  loop(env)
) where
{
fun
loop(xvs: envir): value =
(
case+ xvs of
| list0_nil() =>
  let
  val () =
  println!
  ("interp1_val: x0 = ", x0)
  in
  let
  val () = assertloc(false) in exit(1)
  end
  end

| list0_cons(xv, xvs) =>
  if x0 = xv.0 then xv.1 else loop(xvs)
)
}
end // end of [interp1_val]

(* ****** ****** *)

implement
interp1_tup
  (t0, env) =
let
val-TMtup(ts) = t0
in

VALtup
(list0_map(ts, lam(t) => interp1(t, env)))

end

(* ****** ****** *)

implement
interp1_app
  (t0, env) =
let
val-TMapp(t1, t2) = t0
//
val v1 = interp1(t1, env)
val v2 = interp1(t2, env)
//
in
//
case- v1 of
| VALlam(tf, env1) =>
  let
    val-TMlam(x, t) = tf
  in
    interp1(t, env2) where
    {
      val env2 =
      list0_cons($tup(x, v2), env1)
    }
  end
| VALfix(tf, env1) =>
  let
    val-TMfix(f, x, t) = tf
   in
     interp1(t, env2) where
     {
       val env2 =
       list0_cons( $tup(f, v1), env1 )
       val env2 =
       list0_cons( $tup(x, v2), env2 )
     }
   end
//
end

(* ****** ****** *)

val p1 = TMint(0)
val v1 = interp0(p1)

(* ****** ****** *)

implement main0() =
{
  val () =
  println!("Hello from [lambda1]!")
}

(* ****** ****** *)

(* end of [lambda1.dats] *)

