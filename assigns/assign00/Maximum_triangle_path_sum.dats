(* ****** ****** *)

(*
//
// Here is a C program
//
// http://rosettacode.org/wiki/Maximum_triangle_path_sum#C
//
*)

(* ****** ****** *)
//
// HX: 15 points
//
(* ****** ****** *)
//
extern
fun
Maximum_triangle_path_sum(): int
//
(* ****** ****** *)

(* end of [Maximum_triangle_path_sum.dats] *)
