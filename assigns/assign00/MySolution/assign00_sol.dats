(* ****** ****** *)
//
// How to test
// ./assign00_sol_dats
//
// How to compile:
// myatscc assign00_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign00.dats"

(* ****** ****** *)

implement
factorial(n) =
if n > 0
then n * factorial(n-1) else 1

(* ****** ****** *)

fun int_test_helper(i:int, n:int): int = 
if i+i <= i //overflow
then n+1
else int_test_helper(i+i, n+1)

implement
int_test() = int_test_helper(1, 1)

(* ****** ****** *)

fun gheep_helper(n:int, i:int, a0:int, a1:int):int = 
if i >= n 
then a1 
else gheep_helper(n, i+1, a1, (i+1)*a0*a1) 

implement gheep(n) = 
if n < 2 
then n+1
else gheep_helper(n, 1, 1, 2)

(* ****** ****** *)

fun reverse_intlist (l1: intlist, acc:intlist) : intlist =
case+ l1 of
| intlist_nil() => acc
| intlist_cons(l11, l1s) => reverse_intlist(l1s, intlist_cons(l11, acc))

fun 
intlist_append_helper (l1: intlist, l2: intlist, acc: intlist) : intlist =
case+ l1 of
| intlist_nil() => 
	(case+ l2 of 
	 | intlist_nil() => reverse_intlist(acc, intlist_nil())
	 | intlist_cons(l21, l2s) => 
	   intlist_append_helper(l1, l2s, intlist_cons(l21, acc)))
| intlist_cons(l11, l1s) =>
  intlist_append_helper(l1s, l2, intlist_cons(l11, acc))

fun print_list(l1: intlist): void =
case+ l1 of
| intlist_nil() => print_newline()
| intlist_cons(l11, l1s) => (print!(l11, ", "); print_list(l1s))

implement 
intlist_append(l1, l2) =
intlist_append_helper(l1, l2, intlist_nil())


implement
main0() =
{

val () =
println!
("factorial(10) = ", factorial(10))

val () =
println!
("int_test = ", int_test())

val () =
println!
("ghaap(5) = ", ghaap(5), "\t gheep(5) = ", gheep(5))

// val l1 = g0ofg1($list{int}(1,2,3))
// val l2 = g0ofg1($list{int}(4,5,6))
val l1 = cons(1, cons(2, nil()))
val l2 = cons(3, cons(4, nil()))

val l3 = intlist_append(l1, l2)
val () = println!(("Two lists appended : "))
val () = print_list(intlist_append(l1, l2))
}

 (* end of [main0] *)

(* ****** ****** *)

(* end of [assign00_sol.dats] *)
