#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

#include "./../Maximum_triangle_path_sum.dats"

typedef ll0 = list0(list0(int))

val tri = 
g0ofg1
(
	$list{list0(int)}
	(
	    g0ofg1($list{int}(55)),
		g0ofg1($list{int}(94, 48)),
		g0ofg1($list{int}(95, 30, 96)),
		g0ofg1($list{int}(77, 71, 26, 67)),
		g0ofg1($list{int}(97, 13, 76, 38, 45)),
		g0ofg1($list{int}(7, 36, 79, 16, 37, 68)),
		g0ofg1($list{int}(48, 7, 9, 18, 70, 26, 6)),
		g0ofg1($list{int}(18, 72, 79, 46, 59, 79, 29, 90)),
		g0ofg1($list{int}(20, 76, 87, 11, 32, 7, 7, 49, 18)),
		g0ofg1($list{int}(27, 83, 58, 35, 71, 11, 25, 57, 29, 85)),
		g0ofg1($list{int}(14, 64, 36, 96, 27, 11, 58, 56, 92, 18, 55)),
		g0ofg1($list{int}(2, 90, 3, 60, 48, 49, 41, 46, 33, 36, 47, 23)),
		g0ofg1($list{int}(92, 50, 48, 2, 36, 59, 42, 79, 72, 20, 82, 77, 42)),
		g0ofg1($list{int}(56, 78, 38, 80, 39, 75, 2, 71, 66, 66, 1, 3, 55, 72)),
		g0ofg1($list{int}(44, 25, 67, 84, 71, 67, 11, 61, 40, 57, 58, 89, 40, 56, 36)),
		g0ofg1($list{int}(85, 32, 25, 85, 57, 48, 84, 35, 47, 62, 17, 1, 1, 99, 89, 52)),
		g0ofg1($list{int}(6, 71, 28, 75, 94, 48, 37, 10, 23, 51, 6, 48, 53, 18, 74, 98, 15)),
		g0ofg1($list{int}(27, 2, 92, 23, 8, 71, 76, 84, 15, 52, 92, 63, 81, 10, 44, 10, 69, 93))
	)
)

fun rev_ll0_helper(x: ll0, acc: ll0) : ll0 =
case+ x of
| list0_nil() => acc
| list0_cons(x1, xs) => rev_ll0_helper(xs, list0_cons(x1, acc))

fun rev_ll0(x: ll0): ll0 =
rev_ll0_helper(x, list0_nil())

fun compute_max(x : list0(int)): list0(int) =
case+ x of
| list0_nil() => list0_nil()
| list0_cons(x1, xs) => 
  (
  	case+ xs of 
  	| list0_nil() => list0_nil()
  	| list0_cons(x11, x1s) => 
  	  if (x1 > x11) then list0_cons(x1, compute_max(xs))
  	  else list0_cons(x11, compute_max(xs))
  )

fun print_intlist0(l1: list0(int)): void =
case+ l1 of
| list0_nil() => ()
| list0_cons(l11, l1s) => (print!(l11, " "); print_intlist0(l1s))

// Assuming x and y are equal length
fun element_wise_sum(x: list0(int), y: list0(int)): list0(int) =
case x of
| list0_nil() => list0_nil()
| list0_cons(x1, xs) => 
  (
  case y of 
  | list0_cons(y1, ys) => list0_cons(x1+y1, element_wise_sum(xs, ys))
  ) 

fun first_el(x : list0(int)) : int =
case x of
| list0_cons(x1, xs) => x1

fun process(x: ll0): int =
case x of
| list0_cons(x1, list0_nil()) => first_el(x1)
| list0_cons(x1, xs) => 
  (
  case xs of
  | list0_cons(x11, x1s) => 
    process(list0_cons(element_wise_sum(compute_max(x1), x11), x1s)) 
  )

implement 
Maximum_triangle_path_sum() =
process(rev_ll0(tri))

implement main0() = 
{
val () = println!("Maximum triangle path sum = ", Maximum_triangle_path_sum())
}
