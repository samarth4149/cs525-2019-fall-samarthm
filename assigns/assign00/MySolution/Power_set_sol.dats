#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

#include "./../Power_set.dats"

#define nil list0_nil
#define cons list0_cons

fun print_intlist0(l1: list0(int)): void =
case+ l1 of
| list0_nil() => ()
| list0_cons(l11, l1s) => (print!(l11, " "); print_intlist0(l1s))

fun power_set_helper (xs: list0(int), acc: list0(int)) : void =
case+ xs of
| list0_nil() => (print("[ "); print_intlist0(acc); println!(" ]"))
| cons(x1, x1s) =>  
  (power_set_helper(x1s, cons(x1, acc)); power_set_helper(x1s, acc))

implement 
Power_set(xs) = 
power_set_helper(xs, list0_nil())

implement main0 () =
{
val l1 = g0ofg1($list{int}(1,2,3))
val () = println!("Power set : ")
val () = Power_set(l1)
}