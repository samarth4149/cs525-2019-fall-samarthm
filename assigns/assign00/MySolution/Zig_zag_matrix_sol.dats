#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
#include
"share/HATS/atslib_staload_libats_libc.hats"

#include "./../Zig_zag_matrix.dats"

#define sz(x) i2sz(x)

fun print_int_mat(M : matrix0(int)): void =
let
    val nrow = M.nrow()
    val ncol = M.ncol()

    fun loop(i: int, j: int): void =
        (
            print!(M[i,j], ", ");
            if ncol > j+1 then (loop(i, j+1))
            else (
                print_newline();
                if nrow > i+1 then (loop(i+1, 0))
                else ()
            )
        )
in 
    loop(0, 0)
end

implement Zig_zag_matrix(n) = 
let 
    val M = matrix0_make_elt<int>(n, n, 0)
    val nrow = n
    val ncol = n
    // M: mtrxszref(int), nrow:size_t, ncol:size_t
    fun fill_diag(c:int, i:int, j:int, dir: int): void =
    (
        M[i,j] := c; 
        if ((i = nrow - 1) && (j = ncol - 1)) then ()
        else( 
            if dir < 0 then (
                if ((j = 0) && (i < nrow - 1)) then fill_diag(c+1, i+1, j, 1)
                else (if i = nrow - 1 then fill_diag(c+1, i, j+1, 1)
                      else fill_diag(c+1, i+1, j-1, ~1))
                )  
            else( //dir > 0
                if ((i = 0) && (j < ncol - 1)) then fill_diag(c+1, i, j+1, ~1)
                else (if j = ncol - 1 then fill_diag(c+1, i+1, j, ~1)
                      else fill_diag(c+1, i-1, j+1, 1))
            )
        )
    )
in
    (
        fill_diag(0, 0, 0, 1);
        print_int_mat(M)
    )
end


implement main0() =
{
val () = println!("Zig_zag_matrix(7):")
val () = Zig_zag_matrix(7)
}


