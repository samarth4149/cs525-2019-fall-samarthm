// Used http://rosettacode.org/wiki/Draw_a_sphere#ATS for 
// finding out what typecasts and compare functions are needed

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"
#include
"share/HATS/atslib_staload_libats_libc.hats"

#include "./../Draw_a_sphere.dats"

// staload "libats/libc/SATS/math.sats"
// staload "libats/libc/SATS/stdio.sats"

fun printchar(i: int): void =
case+ i of
| 0 => print (".")
| 1 => print (":")
| 2 => print ("!")
| 3 => print ("*")
| 4 => print ("o")
| 5 => print ("e")
| 6 => print ("&")
| 7 => print ("#")
| 8 => print ("%")
| 9 => print ("@")
| _ => print (" ")


fun normalize(v : (double, double, double)) : (double, double, double) =
let
	val norm = sqrt(v.0 * v.0 + v.1 * v.1 + v.2 * v.2)
in
	(v.0/norm, v.1/norm, v.2/norm)
end

fun dot(v1 : (double, double, double), v2 : (double, double, double))
    : double =
let
	val d = v1.0 * v2.0 + v1.1 * v2.1 + v1.2 * v2.2
	val sgn = gcompare_val_val<double> (d, 0.0)
in
	if sgn < 0 then ~d else 0.0
end


val l = normalize((30.0, 30.0, ~50.0))

fun DrawSphereLoop(i: int, j: int, imax: int, jmax: int, 
				   R: double, k: double, ambient: double) : void =
let
	val x = i + 0.5
	val y = j/2.0 + 0.5
	val sgn = gcompare_val_val<double> (x*x + y*y, R*R)
in
	if (sgn <= 0) then (
		let
			val vec = normalize((x, y, sqrt(R * R - x * x - y * y)))
			val b = pow(dot(l, vec), k) + ambient
			val intensity = 9.0 * (1.0 - b)
			val sgn0 = gcompare_val_val<double> (intensity, 0.0)
			val sgn1 = gcompare_val_val<double> (intensity, 9.0)
		in
			if sgn1 >= 0 then printchar(8) else
			(
				if sgn0 < 0 then printchar(0) else
			 	printchar(g0float2int(intensity))
			)
		end
	)
	else (
		printchar(10)
	);
	if j = jmax then (
		if i = imax then ()
		else (print_newline(); DrawSphereLoop(i+1, g0float2int(floor(2.0 * (~R))), imax, jmax, R, k, ambient)))
	else (DrawSphereLoop(i, j+1, imax, jmax, R, k, ambient)) 
	
end

implement DrawSphere(R, k, ambient) =
DrawSphereLoop(g0float2int(floor(~R)), g0float2int(floor(2.0 * (~R))), 
			   g0float2int(ceil(R)), g0float2int(ceil(2.0 * R)), 
			   R, k, ambient)


implement main0()=
{
	val () = DrawSphere(20.0, 4.0, 0.1)
	val () = print_newline()
	val () = DrawSphere(10.0, 2.0, 0.4)
	val () = print_newline()
}