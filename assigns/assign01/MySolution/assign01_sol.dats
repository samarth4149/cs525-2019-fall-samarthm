#include
"share/atspre_staload.hats"
//
(* ****** ****** *)
//
// #staload
// "./../../mylib2/mylib2.sats"
// #staload
// "./../../mylib2/mylib2.dats"

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

#include "./../assign01.dats"

(* ****** ****** *)

#define nil mylist_nil
#define :: mylist_cons
#define cons mylist_cons

extern
fun
print_term(term): void // stdout
and
prerr_term(term): void // stderr
and
fprint_term(FILEref, term): void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

// 

implement
print_term(tm) =
fprint_term(stdout_ref, tm)
implement
prerr_term(tm) =
fprint_term(stderr_ref, tm)

implement fprint_val<term> = fprint_term
// 

implement
fprint_term(out, tm0) =
(
case+ tm0 of
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tm1) =>
  fprint!(out, "TMlam(", x, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, x, tm1) =>
  fprint!(out, "TMfix(", f, "; ", x, "; ", tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
| TMtup(tms) =>
  fprint!(out, "TMtup(", tms, ")")
| TMprj(tm1, idx) =>
  fprint!(out, "TMprj(", tm1, "; ", idx, ")")
)

(* ****** ****** *)

fun tvar_count_list(tl: termlst, x: vnam): int = 
case+ tl of
| mylist_nil() => 0
| mylist_cons(t1, ts) => tvar_count(t1, x) + tvar_count_list(ts, x)

// This doesn't handle index out of bounds yet
fun proj_i(tl:termlst, i:int): term = 
case+ i of
| 0 => let val- mylist_cons(t1, ts) = tl in t1 end
| _ => let val- mylist_cons(t1, ts) = tl in proj_i(ts, i-1) end

implement tvar_count(t0, x) =
case+ t0 of
| TMint(_) => 0
| TMvar(y) => if y = x then 1 else 0
| TMlam(y, t1) => if y = x then 0 else tvar_count(t1, x)
| TMapp(t1, t2) => tvar_count(t1, x) + tvar_count(t2, x)
| TMifz(t1, t2, t3) => tvar_count(t1, x) + tvar_count(t2, x) + tvar_count(t3, x)
| TMfix(f, y, t1) => 
if f = x then 0 
else
(if y = x then 0 else tvar_count(t1, x))
| TMopr(_, tl) => tvar_count_list(tl, x)
| TMtup(tl) => tvar_count_list(tl, x)
// case of projection when term is not a tuple not handled
| TMprj(t1, i) => tvar_count(t1, x)


(* ****** ****** *)
extern 
fun tvar_count_all_list(tl: termlst): int

extern 
fun tvar_count_all(t0: term): int

implement 
tvar_count_all_list(tl) = 
case+ tl of
| mylist_nil() => 0
| mylist_cons(t1, ts) => tvar_count_all(t1) + tvar_count_all_list(ts)

implement 
tvar_count_all(t0) =
case+ t0 of
| TMint(_) => 0
| TMvar(_) => 1
| TMlam(y, t1) => tvar_count_all(t1) - tvar_count(t1, y)
| TMapp(t1, t2) => tvar_count_all(t1) + tvar_count_all(t2)
| TMifz(t1, t2, t3) => tvar_count_all(t1) + tvar_count_all(t2) + tvar_count_all(t3)
| TMfix(f, y, t1) => tvar_count_all(t1) - tvar_count(t1, y) - tvar_count(t1, f)
| TMopr(_, tl) => tvar_count_all_list(tl)
| TMtup(tl) => tvar_count_all_list(tl)
| TMprj(t1, i) => tvar_count_all(t1)

implement term_is_closed(t0) =
if tvar_count_all(t0) = 0 then true else false

(* ****** ****** *)

extern
fun
subst0
(tm0: term, x0: vnam, sub: term): term

implement
subst0
(tm0, x0, sub) =
let
fun
helper(tm: term): term = subst0(tm, x0, sub)
in
case+ tm0 of
| TMint _ => tm0
| TMvar(x1) =>
  if
  x0 = x1 then sub else tm0
| TMlam(x1, tm1) =>
  if
  x0 = x1
  then tm0
  else TMlam(x1, helper(tm1))
| TMapp(tm1, tm2) =>
  TMapp(helper(tm1), helper(tm2))
| TMifz(tm1, tm2, tm3) =>
  TMifz(helper(tm1), helper(tm2), helper(tm3))
| TMfix(f0, x1, tm1) =>
  if
  x0 = f0
  then tm0
  else
  (
  if
  x0 = x1
  then tm0
  else TMfix(f0, x1, helper(tm1))
  )
| TMopr(opr, tms) =>
  TMopr(opr, mylist_map<term><term>(tms, lam(tm) => helper(tm)))
| TMtup(tms) =>
  TMtup(mylist_map<term><term>(tms, lam(tm) => helper(tm)))
| TMprj(tm1, idx) =>
  TMprj(helper(tm1), idx)
end

extern
fun interp0_opr(t0: term) : term


implement
interp0_opr(tm0) =
let
val-
TMopr(opr, tms) = tm0
//
val tms =
mylist_map<term><term>(tms, lam(tm) => interp0(tm))
//
in
  case+ opr of
  | "+" =>
    let
      val-mylist_cons(tm1, tms) = tms
      val-mylist_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1 + i2)
    end
  | "-" =>
    let
      val-mylist_cons(tm1, tms) = tms
      val-mylist_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1 - i2)
    end
  | "*" =>
    let
      val-mylist_cons(tm1, tms) = tms
      val-mylist_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1 * i2)
    end
  | "/" =>
    let
      val-mylist_cons(tm1, tms) = tms
      val-mylist_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1 / i2)
    end
  | ">" =>
    let
      val-mylist_cons(tm1, tms) = tms
      val-mylist_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      if i1 > i2 then TMint(1) else TMint(0)
    end
  | "<" =>
    let
      val-mylist_cons(tm1, tms) = tms
      val-mylist_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      if i1 < i2 then TMint(1) else TMint(0)
    end
  | "=" =>
  	let
      val-mylist_cons(tm1, tms) = tms
      val-mylist_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      if i1 = i2 then TMint(1) else TMint(0)
    end
  | "%" =>
    let
      val-mylist_cons(tm1, tms) = tms
      val-mylist_cons(tm2, tms) = tms
      val-TMint(i1) = tm1 and TMint(i2) = tm2
    in
      TMint(i1%i2)
    end
  | _ => let
      val () =
      prerrln!("interp0: TMopr: opr is ", opr) 
    in
      assertloc(false); exit(1)
    end
end



implement
interp0(tm0) =
(
case+ tm0 of
| TMint _ => tm0
| TMvar _ => tm0
| TMlam(x0, tm1) => tm0
| TMapp(tm1, tm2) =>
  let
    val tm1 = interp0(tm1)
    val tm2 = interp0(tm2)
  in
    case+ tm1 of
    | TMlam(x1, tm11) =>
      interp0(subst0(tm11, x1, tm2))
    | _(* non-TMlam *) => tm0
  end
| TMifz(tm1, tm2, tm3) =>
  let
  val tm1 = interp0(tm1)
  in
    case- tm1 of
    | TMint(i) =>
      if i = 0 then interp0(tm2) else interp0(tm3)
  end
| TMfix(f0, x1, tm1) =>
  TMlam(x1, subst0(tm1, f0, tm0))
| TMopr(opr, tms) => interp0_opr(tm0)
| TMtup(tms) =>
  TMtup(mylist_map<term><term>(tms, lam(tm) => interp0(tm)))
| TMprj(tm1, idx) =>
  let
    val tm1 = interp0(tm1)
    val-TMtup(tms) = tm1 in proj_i(tms, idx)
  end
)


(* ****** ****** *)

implement main0() =
{
val x = TMvar("x")
val y = TMvar("y")
val z = TMvar("z")

// using TMint(1) for true
val check_prime_helper = TMfix("f", "ni", 
	TMifz(
		TMopr("=", n::i::nil()), 
		TMifz(
			TMopr("%", n::i::nil()), 
			TMint(0),
			TMapp(f, TMtup(n :: TMopr("+", i::TMint(1)::nil()) :: nil()))
		),
		TMint(1)
  ))
where
{
	val f = TMvar("f")
	val ni = TMvar("ni")
	val n = TMprj(ni, 0)
	val i = TMprj(ni, 1)
}

val check_prime = TMlam("z", 
	TMifz(
    TMopr("<", z::TMint(2)::nil()), 
    TMapp(check_prime_helper, TMtup(z::TMint(2)::nil())),
    TMint(0))
  )


val () = println!(tvar_count(TMapp(TMapp(x, z), TMapp(y, z)), "z")) 
val () = println!(tvar_count(TMlam("y", TMlam("z", TMapp(TMapp(x, z), TMapp(y, z)))), "z")) 
val () = println!(term_is_closed(TMlam("y", TMlam("z", TMapp(TMapp(x, z), TMapp(y, z)))))) 
val () = println!(term_is_closed(TMlam("x", TMlam("y", TMlam("z", TMapp(TMapp(x, z), TMapp(y, z)))))))

val () = println!("IsPrime(1) = ", interp0(TMapp(check_prime, TMint(1))))
val () = println!("IsPrime(43) = ", interp0(TMapp(check_prime, TMint(43))))
val () = println!("IsPrime(42) = ", interp0(TMapp(check_prime, TMint(42))))
}