(* ****** ****** *)

#include
"share\
/atspre_staload.hats"
#include
"share\
/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload "./../../../mylib/mylib.sats"
#staload "./../../../mylib/mylib.dats"

(* ****** ****** *)

typedef tnam = string
typedef vnam = string
typedef opnm = string

(* ****** ****** *)
//
datatype type =
  | TPbas of tnam
  | TPfun of
    (type(*arg*), type(*res*))
  | TPtup of
    (type(*fst*), type(*snd*))
//
(* ****** ****** *)

typedef
typelst = mylist(type)

(* ****** ****** *)

datatype ctype =
  | CTYPE of (typelst, type)

(* ****** ****** *)

typedef
ctypeopt = myoptn(ctype)

(* ****** ****** *)

extern
fun
opnm_get_ctype(x0: opnm): ctypeopt
extern
fun
opnm_set_ctype(x0: opnm, ct: ctype): void

(* ****** ****** *)

local

typedef
xcts =
mylist
($tup(opnm, ctype))
val
theCTmap =
ref<xcts>(mylist_nil())

in (*in-of-local*)

implement
opnm_get_ctype(x0) =
(
  loop(theCTmap[])
) where
{
fun
loop
(xcts: xcts): ctypeopt =
(
case+ xcts of
| mylist_nil() =>
  myoptn_none()
| mylist_cons(xct0, xcts) =>
  if
  x0 = xct0.0
  then myoptn_some(xct0.1) else loop(xcts)
)
} // opnm_get_ctype

implement
opnm_set_ctype(x0, ct) =
let
val xcts = theCTmap[]
in
  theCTmap[] := mylist_cons($tup(x0, ct), xcts)
end // opnm_set_ctype

end // end of [local]

(* ****** ****** *)
//
extern
fun
eq_type_type:
(type, type) -> bool
overload = with eq_type_type
//
(* ****** ****** *)

extern
fun
print_type(type): void // stdout
and
prerr_type(type): void // stderr
and
fprint_type(FILEref, type): void

overload print with print_type
overload prerr with prerr_type
overload fprint with fprint_type

(* ****** ****** *)

extern
fun
print_ctype(ctype): void // stdout
and
prerr_ctype(ctype): void // stderr
and
fprint_ctype(FILEref, ctype): void

overload print with print_ctype
overload prerr with prerr_ctype
overload fprint with fprint_ctype

(* ****** ****** *)

datatype expr = 
//
  | TMint of int
  | TMstr of string
//
  | TMvar of vnam
  | TMlam of
    (vnam, type, expr)
  | TMapp of (expr, expr)
//
  | TMifz of
    (expr, expr, expr)
//
  | TMfix of
    (vnam, type, vnam, expr)
//
  | TMopr of (opnm, exprlst)
//
  | TMtup of (expr, expr)
  | TMfst of expr | TMsnd of expr
//
  | TMlet of (vnam, expr, expr) // let x = t1 in t2 end
//
where exprlst = mylist(expr)

(* ****** ****** *)

extern
fun
print_expr(expr): void // stdout
and
prerr_expr(expr): void // stderr
and
fprint_expr(FILEref, expr): void

overload print with print_expr
overload prerr with prerr_expr
overload fprint with fprint_expr

(* ****** ****** *)

implement
eq_type_type =
lam(tp1, tp2) =>
(
case+
(tp1, tp2) of
| (TPbas nm1,
   TPbas nm2) => (nm1 = nm2)
| (TPfun(tp11, tp12), 
   TPfun(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (TPtup(tp11, tp12), 
   TPtup(tp21, tp22)) =>
   tp11 = tp21 && tp12 = tp22
| (_, _) => false
)

(* ****** ****** *)
//
implement
fprint_val<type> = fprint_type
implement
fprint_val<expr> = fprint_expr
//
(* ****** ****** *)

implement
print_type(tp) =
fprint_type(stdout_ref, tp)
implement
prerr_type(tp) =
fprint_type(stderr_ref, tp)

(* ****** ****** *)

implement
fprint_type(out, tp0) =
(
case+ tp0 of
| TPbas(nm) =>
  fprint!(out, "TPbas(", nm, ")")
| TPfun(tp1, tp2) =>
  fprint!(out, "TPfun(", tp1, ", ", tp2, ")")
| TPtup(tp1, tp2) =>
  fprint!(out, "TPtup(", tp1, ", ", tp2, ")")
)

(* ****** ****** *)

implement
print_ctype(tp) =
fprint_ctype(stdout_ref, tp)
implement
prerr_ctype(tp) =
fprint_ctype(stderr_ref, tp)

(* ****** ****** *)

implement
fprint_ctype(out, ct0) =
(
case+ ct0 of
| CTYPE(tps, tp1) =>
  fprint!(out, "CTYPE(", tps, "; ", tp1, ")")
)

(* ****** ****** *)

implement
print_expr(tm) =
fprint_expr(stdout_ref, tm)
implement
prerr_expr(tm) =
fprint_expr(stderr_ref, tm)

(* ****** ****** *)

implement
fprint_val<expr> = fprint_expr

(* ****** ****** *)

implement
fprint_expr(out, tm0) =
(
case+ tm0 of
| TMint(x) =>
  fprint!(out, "TMint(", x, ")")
| TMstr(x) =>
  fprint!(out, "TMstr(", x, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam(x, tp0, tm1) =>
  fprint!
  ( out
  , "TMlam(", x, "; ", tp0, "; ", tm1, ")")
| TMapp(tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz(tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
| TMfix(f, tp1, x, tm1) =>
  fprint!(out, "TMfix(", f, "; ", tp1, "; ", x, "; ", tm1, ")")
| TMopr(opr, tms) =>
  fprint!(out, "TMopr(", opr, "; ", tms, ")")
//
| TMfst(tm1) =>
  fprint!(out, "TMfst(", tm1, ")")
| TMsnd(tm1) =>
  fprint!(out, "TMsnd(", tm1, ")")
| TMtup(tm1, tm2) =>
  fprint!(out, "TMtup(", tm1, "; ", tm2, ")")
//
| TMlet(x0, tm1, tm2) =>
  fprint!(out, "TMlet(", x0, "; ", tm1, "; ", tm2, ")")
)

(* ****** ****** *)

exception IllTyped0 of ()

(* ****** ****** *)

typedef tctx =
mylist($tup(vnam, type))

(* ****** ****** *)

extern
fun oftype0(expr): type
extern
fun oftype1(tctx, expr): type
extern
fun test_tmlst(tctx, mylist(expr), typelst) : bool

(* ****** ****** *)

val TPint = TPbas("int")
val TPstr = TPbas("string")
val TPbool = TPbas("bool")
val TPvoid = TPbas("void")

(* ****** ****** *)

implement
oftype0(tm0) =
oftype1(mylist_nil(), tm0)

fun var_get_type(tctx0: tctx, x0: vnam) : type =
case tctx0 of
(* should ideally be raised in the line calling this function *)
| mylist_nil() => $raise IllTyped0() 
| mylist_cons(vt0, vts) => 
  if vt0.0 = x0 then vt0.1 else var_get_type(vts, x0)

implement test_tmlst(tctx0, tms, tps) =
case+ tms of
| mylist_nil() =>
  (case+ tps of
  | mylist_nil() => true
  | _ => false
  )
| mylist_cons(tm1, tms1) =>
  (case+ tps of
  | mylist_nil() => false
  | mylist_cons(tp1, tps1) =>
    oftype1(tctx0, tm1) = tp1 && test_tmlst(tctx0, tms1, tps1)
  )

implement
oftype1(tctx0, tm0) =
(
case- tm0 of
//
| TMint _ => TPint
| TMstr _ => TPstr
//
  | TMvar(x0) => var_get_type(tctx0, x0)
  | TMlam
    (x0, tp1, tm2) =>
    let
      val
      tctx1 =
      mylist_cons
      ($tup(x0, tp1), tctx0)
    in
      TPfun(tp1, oftype1(tctx1, tm2))
    end
//
  | TMapp(tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    in
      case- tp1 of
      | TPfun(tp11, tp12) =>
        if
        tp11 = tp2
        then tp12 else $raise IllTyped0()
    end
//
  | TMifz(tm1, tm2, tm3) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    val tp3 = oftype1(tctx0, tm3)
    in
      if
      tp1 = TPint
      then
      (
      if tp2 = tp3 then tp2 else $raise IllTyped0()
      )
      else $raise IllTyped0()
    end
//

  | TMfix(f0, tp0, x0, tm1) =>
  // return the type of f0 after checks
    (case+ tp0 of
    | TPfun(tp_arg, tp_res) => 
      let 
        val tctx1 = 
        mylist_cons($tup(f0, tp0), mylist_cons($tup(x0, tp_arg), tctx0))
      in
        if tp_res = oftype1(tctx1, tm1) then tp0
        else $raise IllTyped0() 
      end
    | _ => $raise IllTyped0()
    )

  | TMopr(opr, tms) => 
    let 
      val- myoptn_some(CTYPE(ct00, ct01)) = opnm_get_ctype(opr)
    in
      if test_tmlst(tctx0, tms, ct00) then ct01
      else $raise IllTyped0()
    end
//
  | TMtup(tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tp2 = oftype1(tctx0, tm2)
    in
      TPtup(tp1, tp2)
    end
//
  | TMfst(tm1) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    in
      case- tp1 of TPtup(tp11, _) => tp11
    end
  | TMsnd(tm1) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    in
      case- tp1 of TPtup(_, tp12) => tp12
    end
//
  | TMlet(x0, tm1, tm2) =>
    let
    val tp1 = oftype1(tctx0, tm1)
    val tctx1 =
    mylist_cons($tup(x0, tp1), tctx0)
    in
       oftype1(tctx1, tm2)
    end
)

(* ****** ****** *)
//
#define nil mylist_nil
//
#define :: mylist_cons
#define cons mylist_cons
//
(* ****** ****** *)

val () =
opnm_set_ctype
("+", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("-", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("*", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("/", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("=", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("<", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
(">", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
("<=", CTYPE(TPint :: TPint :: nil(), TPint))
val () =
opnm_set_ctype
(">=", CTYPE(TPint :: TPint :: nil(), TPint))

(* ****** ****** *)

(*

*)

(* ****** ****** *)

implement main0() = 
{
  val TMint1 = TMint(1)
  val () =
  println!("oftype(TMint1) = ", oftype0(TMint1))
  // val () =
  // println!("oftype(TMapp...) = ", oftype0(TMapp(TMint1, TMint1)))
  

  val x = TMvar("x")
  val coins = TMlam("x", TPint,
    TMifz(
      TMopr("=", x::TMint(0)::nil()),
      TMifz(
        TMopr("=", x::TMint(1)::nil()),
        TMifz(
          TMopr("=", x::TMint(2)::nil()),
          TMifz(
            TMopr("=", x::TMint(3)::nil()),
            TMint(~1), (* invalid *)
            TMint(25)
          ),
          TMint(10)
        ),
        TMint(5)
      ),
      TMint(1)
    )
  )
  val aux = ( TMfix("f", TPfun(TPtup(TPint, TPint), TPint), "sn", 
    TMifz(TMopr("<=", s::TMint(0)::nil()), 
          TMifz(TMopr("<", n::TMint(0)::nil()),
                TMopr("+", 
                  TMapp(f, TMtup(s, TMopr("-", n::TMint(1)::nil())))
                  :: TMapp(f, TMtup(TMopr("-", s::TMapp(coins, n)::nil()), n))
                  :: nil()),
                TMint(0)),
          TMifz(TMopr(">=", s::TMint(0)::nil()),
                TMint(0),
                TMint(1))))
  )
  where 
  {
    val f = TMvar("f")
    val sn = TMvar("sn")
    val s = TMfst(sn)
    val n = TMsnd(sn)
  } 

  val cc = TMlam("x", TPint, TMapp(aux, TMtup(x, TMint(3))))

  // val () =
  // println!("oftype(x) = ", oftype0(x))
  val () =
  println!("oftype(coins) = ", oftype0(coins))
  val () =
  println!("oftype(aux) = ", oftype0(aux))
  val () =
  println!("oftype(cc) = ", oftype0(cc))

}

(* ****** ****** *)

(* end of [lambda2.dats] *)

