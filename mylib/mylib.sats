(* ****** ****** *)

datatype
myoptn(a:t0ype) =
| myoptn_none of ()
| myoptn_some of (a)

(* ****** ****** *)
//
fun
{a:t0ype}
print_myoptn: myoptn(a) -> void
fun
{a:t0ype}
prerr_myoptn: myoptn(a) -> void
//
fun
{a:t0ype}
fprint_myoptn
(out: FILEref, xs: myoptn(a)): void
fun{}
fprint_myoptn$sep(out: FILEref): void
//
overload print with print_myoptn
overload prerr with prerr_myoptn
overload fprint with fprint_myoptn
//
(* ****** ****** *)

datatype
mylist(a:t0ype) =
| mylist_nil of ()
| mylist_cons of (a, mylist(a))

(* ****** ****** *)
//
fun
{a:t0ype}
print_mylist: mylist(a) -> void
fun
{a:t0ype}
prerr_mylist: mylist(a) -> void
//
fun
{a:t0ype}
fprint_mylist
(out: FILEref, xs: mylist(a)): void
fun{}
fprint_mylist$sep(out: FILEref): void
//
overload print with print_mylist
overload prerr with prerr_mylist
overload fprint with fprint_mylist
//
(* ****** ****** *)

fun
{a:t0ype}
mylist_length
(xs: mylist(a)): int

(* ****** ****** *)

fun
{a:t0ype}
mylist_reverse
(xs: mylist(a)): mylist(a)

(* ****** ****** *)

fun
{a:t0ype}
mylist_revapp
(mylist(a), mylist(a)): mylist(a)
fun
{a:t0ype}
mylist_append
(mylist(a), mylist(a)): mylist(a)
fun
{a:t0ype}
mylist_extend
(mylist(a), a): mylist(a)

(* ****** ****** *)

fun
{a:t0ype}
mylist_get_at(xs: mylist(a), n: int): (a)

(* ****** ****** *)
//
fun{}
mystring_append
  (s1: string, s2: string): string
//
(* ****** ****** *)

fun
{a:t0ype}
mylist_rev(xs: mylist(a)): mylist(a)

fun
{a1:t0ype}{a2:t0ype}
mylist_map(xs: mylist(a1), f: a1-<cloref1>a2): mylist(a2) 

(* end of [mylib.sats] *)
