(*
** For your
** final project
*)

(* ****** ****** *)

#staload
"./../mylib/mylib.sats"

(* ****** ****** *)

typedef t0nam = string
typedef v0nam = string
typedef op0nm = string

(* ****** ****** *)

abstype tvar_type = ptr
typedef tvar = tvar_type

(* ****** ****** *)
//
datatype
type =
//
  | TPbas of t0nam // base
//
  | TPext of tvar // unify
//
  | TPtup of // tuples
    (type(*fst*), type(*snd*))
//
  | TPfun of // functions
    (type(*arg*), type(*res*))
//
typedef typeopt = myoptn(type)
typedef typelst = mylist(type)
//
(* ****** ****** *)
//
datatype
ctype = CTP of (typelst, type)
//
(* ****** ****** *)

datatype
d0ecl =
  | D0Lbnd of (v0nam, t0erm)
(*
val x = 1
val f = lam x => x + x
*)
and t0erm =
//
  | T0Mint of int
  | T0Mbool of bool
(*
  | T0Mfloat of double
*)
  | T0Mstring of string
//
  | T0Mvar of v0nam
  | T0Mlam of
    (v0nam, typeopt, t0erm)
  | T0Mapp of (t0erm, t0erm)
//
  | T0Mift of (t0erm, t0erm, t0ermopt)
//
  | T0Mopr of (op0nm, t0ermlst)
//
  | T0Mtup of (t0erm, t0erm)
  | T0Mfst of t0erm | T0Msnd of t0erm
//
  | T0Mann of (t0erm, type) // type-annotation
//
  | T0Mlet of
    (d0eclist(*local*), t0erm(*scope*)) // local bindings
//
  | T0Mfix of
    ( v0nam(*f*)
    , v0nam(*x*)
    , typeopt(*arg*)
    , typeopt(*res*), t0erm) // Y(lam f.lam x.<body>)
//
where
d0eclist = mylist(d0ecl)
and
t0ermlst = mylist(t0erm)
and
t0ermopt = myoptn(t0erm)

(* ****** ****** *)
//
// Resolving binding/scoping issues
//
(* ****** ****** *)

abstype v1nam_type = ptr
typedef v1nam = v1nam_type

(* ****** ****** *)

datatype
d1ecl =
  | D1Lbnd of (v1nam, t1erm)
and t1erm =
//
  | T1Mint of int
  | T1Mbool of bool
(*
  | T1Mfloat of double
*)
  | T1Mstring of string
//
  | T1Mvar of v1nam
  | T1Mlam of
    (v1nam, typeopt, t1erm)
  | T1Mapp of (t1erm, t1erm)
//
  | T1Mift of (t1erm, t1erm, t1ermopt)
//
  | T1Mopr of (op0nm, t1ermlst)
//
  | T1Mtup of (t1erm, t1erm)
  | T1Mfst of t1erm | T1Msnd of t1erm
//
  | T1Mann of (t1erm, type) // type-annotation
//
  | T1Mlet of
    (d1eclist(*local*), t1erm(*scope*)) // local bindings
//
  | T1Mfix of
    ( v1nam(*f*)
    , v1nam(*x*)
    , typeopt(*arg*)
    , typeopt(*res*), t1erm) // Y(lam f.lam x.<body>)
//
where
d1eclist = mylist(d1ecl)
and
t1ermlst = mylist(t1erm)
and
t1ermopt = myoptn(t1erm)

(* ****** ****** *)

fun trans01_decl : d0ecl -> d1ecl
fun trans01_term : t0erm -> t1erm

(* ****** ****** *)

abstype t2erm_type = ptr
typedef t2erm = t2erm_type

(*
absimpl
t2erm_type =
$rec{
  t2erm_type= type
, t2erm_node= t2erm_node
}
*)

(* ****** ****** *)

datatype
d2ecl =
  | D2Lbnd of (v1nam, t2erm)
and t2erm_node =
//
  | T2Mint of int
  | T2Mbool of bool
(*
  | T2Mfloat of double
*)
  | T2Mstring of string
//
  | T2Mvar of v1nam
  | T2Mlam of
    (v1nam, typeopt, t2erm)
  | T2Mapp of (t2erm, t2erm)
//
  | T2Mift of (t2erm, t2erm, t2ermopt)
//
  | T2Mopr of (op0nm, t2ermlst)
//
  | T2Mtup of (t2erm, t2erm)
  | T2Mfst of t2erm | T2Msnd of t2erm
//
  | T2Mann of (t2erm, type) // type-annotation
//
  | T2Mlet of
    (d2eclist(*local*), t2erm(*scope*)) // local bindings
//
  | T2Mfix of
    ( v1nam(*f*)
    , v1nam(*x*)
    , typeopt(*arg*)
    , typeopt(*res*), t2erm) // Y(lam f.lam x.<body>)
//
  | T2Mcast of (t2erm, type) // for internalization of type-errors
//
where
d2eclist = mylist(d2ecl)
and
t2ermlst = mylist(t2erm)
and
t2ermopt = myoptn(t2erm)

(* ****** ****** *)

datatype t3reg =
| T3REG of stamp
where stamp = int

(* ****** ****** *)

fun
t3reg_new(): t3reg

(* ****** ****** *)

datatype
t3val =
| T3Vint of int
| T3Vstr of string
| T3Vfun of string
| T3Vtmp of t3reg
| T3Varg of string
| T3Venv of (string, int)
| T3Vclo of (string, t3vals(*env*), t3inss(*body*), t3val(*res*))

and
t3ins =
| T3Imov of (t3reg, t3val)
| T3Itup of (t3reg, t3val, t3val)
| T3Ical of (t3reg, t3val, t3val)
| T3Iopr of (t3reg, op0nm, t3vals)
| T3Iift of (t3val, t3inss, t3inss)

where
t3vals = mylist(t3val)
and
t3inss = mylist(t3ins)

(* ****** ****** *)

abstype
t3env_type = ptr
typedef
t3env = t3env_type

fun
compile :
(t3env, t2erm) -> (t3inss, t3val)

fun
compile_var :
(t3env, t2erm) -> t3val
and
compile_lam :
(t3env, t2erm) -> t3val
and
compile_fix :
(t3env, t2erm) -> t3val

(* ****** ****** *)

fun emit_tval : t3val -> void
fun emit_tins : t3ins -> void
fun emit_tinss : t3inss -> void

(* ****** ****** *)

(* end of [project.sats] *)
