### Compilation
For compiling the compiler, run 

```bash
make libxatsopt
make project
```

This would create an executable `project` in the current directory.

### Running the compiler
For running the compiler, (the following command is an example for a sample source program in TEST/fact.dats)

```bash
./project TEST/fact.dats
```

This would create a C target file `out.c` as the output of the compiler.
This can then be compiled into an executable using gcc and run as 

```bash 
gcc out.c
./a.out
```