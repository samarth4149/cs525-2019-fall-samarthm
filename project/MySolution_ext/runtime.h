/*
// A basic runtime for lambda
*/

/* ****** ****** */

#include <stdio.h>
#include <stdlib.h>

/* ****** ****** */

#define TAGint 1
#define TAGbool 2
#define TAGstr 3
#define TAGfunclo 4
#define TAGtup 5
#define TAGlist 6
#define TAGarray 7

/* ****** ****** */

typedef
struct{ int tag; } lamval_;

/* ****** ****** */

typedef
struct{ int tag; int data; } lamval_int_;
typedef
struct{ int tag; int data; } lamval_bool_;
typedef
struct{ int tag; char *data; } lamval_string_;

typedef
struct{ int tag; int fenv_len; void **fenv; } lamval_funclo_;

typedef
struct{ int tag; void *fst; void *snd; } lamval_tup_;

/* ****** ****** */

typedef lamval_ *lamval;
typedef lamval_int_ *lamval_int;
typedef lamval_bool_ *lamval_bool;
typedef lamval_string_ *lamval_string;
typedef lamval_funclo_ *lamval_funclo;
typedef lamval_tup_ *lamval_tup;

/* ****** ****** */

/*
typedef
struct{ int tag; lamval head; lamval tail; } lamval_list_;

typedef
struct{ int tag; size_t size; lamval data[]; } lamval_array_;
*/

/* ****** ****** */

#define \
FUNCLO_ENVLEN(cfn) \
(((lamval_funclo)cfn)->fenv_len)
#define \
FUNCLO_ENV(cfn) \
(((lamval_funclo)cfn)->fenv)
#define \
FUNCLO_FUN(cfn) \
((lamval(*)(lamval, void**, int))(((lamval_funclo)cfn)->fenv[0]))

#define \
FUNCLO_APP(cfn, arg) \
FUNCLO_FUN(cfn)(arg, FUNCLO_ENV(cfn), FUNCLO_ENVLEN(cfn))

/* ****** ****** */

// NOT SUPPORTED
// #if(0)
// #define      \
// FUNCLO_FUN2(cfn) \
// ((lamval(*)(lamval, lamval, void**))(((lamval_funclo)cfn)->fenv[0]))
// #define \
// FUNCLO_APP2(cfn, arg1, arg2) \
// FUNCLO_FUN2(cfn)(arg1, arg2, FUNCLO_ENV(cfn))
// #endif

/* ****** ****** */

lamval
T3Vint(int i)
{
  lamval_int p0;
  p0 = malloc(sizeof(lamval_int_));
  p0->tag = TAGint; p0->data = i; return (lamval)p0;
}

lamval
T3Vbool(int i)
{
  lamval_bool p0;
  p0 = malloc(sizeof(lamval_bool_));
  p0->tag = TAGbool; p0->data = i; return (lamval)p0;
}

lamval
T3Vstr(char *cs)
{
  lamval_string p0;
  p0 = malloc(sizeof(lamval_string_));
  p0->tag = TAGstr; p0->data = cs; return (lamval)p0;
}

/* ****** ****** */

#define T3Vreg(reg) reg
#define T3Varg(arg) arg

// NOTE : fenv_len does not include the function pointer
lamval
T3Vclo(int fenv_len, lamval *env)
{
  lamval_funclo p0;
  p0 = malloc(sizeof(lamval_funclo_));
  p0->tag = TAGfunclo; p0->fenv_len = fenv_len; p0->fenv = (void**)env; return (lamval)p0;
}

lamval
T3Vtup(lamval x, lamval y){
  lamval_tup p0;
  p0 = malloc(sizeof(lamval_tup_));
  p0->tag = TAGtup;
  p0->fst = (void*) x;
  p0->snd = (void*) y;
  return (lamval)p0;
}


/* ****** ****** */

#define T3Iift(x) if(((lamval_bool)x)->data)

/* ****** ****** */

lamval
T3Vopr_add(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data + ((lamval_int)y)->data);
}

lamval
T3Vopr_sub(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data - ((lamval_int)y)->data);
}

lamval
T3Vopr_mul(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data * ((lamval_int)y)->data);
}

lamval
T3Vopr_div(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data / ((lamval_int)y)->data);
}

lamval
T3Vopr_mod(lamval x, lamval y)
{
  return
  T3Vint(((lamval_int)x)->data % ((lamval_int)y)->data);
}

lamval
T3Vopr_lt(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data < ((lamval_int)y)->data);
}
lamval
T3Vopr_gt(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data > ((lamval_int)y)->data);
}
lamval
T3Vopr_eq(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data == ((lamval_int)y)->data);
}
lamval
T3Vopr_lte(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data <= ((lamval_int)y)->data);
}
lamval
T3Vopr_gte(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data >= ((lamval_int)y)->data);
}
lamval
T3Vopr_neq(lamval x, lamval y)
{
  return
  T3Vbool(((lamval_int)x)->data != ((lamval_int)y)->data);
}
lamval
T3Vopr_fst(lamval x){
  return
  (lamval)(((lamval_tup)x)->fst);
}
lamval
T3Vopr_snd(lamval x){
  return
  (lamval)(((lamval_tup)x)->snd);
}

lamval
T3Vopr_print(lamval x)
{
  /*
  assert(x->tag == TAGint);
  assert(y->tag == TAGint);
  */
  switch(x->tag)
    {
    case TAGbool: printf("BOOL(%s)\n", ((lamval_bool)x)->data); break;
    case TAGint: printf("%i", ((lamval_int)x)->data); break;
    case TAGstr: printf("%s", ((lamval_string)x)->data); break;
    case TAGfunclo: printf("<LAMVAL_CLO>"); break;
    case TAGtup: printf("<LAMVAL_TUP>"); break;
    default: printf("What???");
    }
}

int get_bool(lamval x){
  return ((lamval_bool)x)->data;
}

void loop_copy(lamval *clo_env, lamval *env, int i, int j, int len){
  int it;
  for (it=0; it<len; it = it + 1){
    clo_env[i+it] = env[j+it];
  }
}
/* ****** ****** */

/* end of [runtime.h] */
