(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)
//
(* ****** ****** *)

#staload
"./../../mylib/mylib.sats"
#staload _ =
"./../../mylib/mylib.dats"

#staload
UN =
"prelude/SATS/unsafe.sats"

(* ****** ****** *)

#define nil mylist_nil
#define :: mylist_cons
#define cons mylist_cons

#define none myoptn_none
#define some myoptn_some

(* ****** ****** *)

#staload "./project.sats"

(* ****** ****** *)

exception Illtyped2 of ()
exception OprTypeNotFound of ()



implement 
print_t2erm(x) = fprint_t2erm(stdout_ref, x)

implement
fprint_val<type0> = fprint_type0
implement
fprint_val<type2> = fprint_type2
implement
fprint_val<tmvar> = fprint_tmvar

implement
fprint_val<t2erm> = fprint_t2erm
implement
fprint_val<t2erm_node> = fprint_t2erm_node
implement
fprint_val<t2dcl> = fprint_t2dcl

(* ****** ****** *)

implement 
type_trans02(t0p) =
case+ t0p of
| T0Pbas(x) => T2Pbas(x)
| T0Ptup(t0p1, t0p2) => 
  T2Ptup(type_trans02(t0p1), type_trans02(t0p2))
| T0Pfun(t0p1, t0p2) =>
  T2Pfun(type_trans02(t0p1), type_trans02(t0p2))

(* ****** ****** *)

absimpl tpext_type = ref(type2opt)

implement
tpext_new() =
ref<type2opt>(none)

implement
tpext_get_type(X) = X[]
implement
tpext_set_type(X, T) = (X[] := some(T))

implement
type2_new() =
T2Pext(tpext_new())

// TODO: check if eval needs to go 
// into T2Ptup and T2Pfun and evaluate
fun
type_eval
(T: type2): type2 =
(
case+ T of
| T2Pext(X) =>
  let
  val opt = X.type()
  in
  case+ opt of
  | none() => T
  | some(T) => type_eval(T)
  end
| _ (* non-TPext *) => T
)

overload eval with type_eval

implement
eq_tpext_tpext
  (X, Y) =
(
  $UN.cast{ptr}(X)
  =
  $UN.cast{ptr}(Y)
)

(* ****** ****** *)

extern
fun
unify:
(type2, type2) -> bool

extern
fun
unify_if_present:
(type2, type0opt) -> bool

extern
fun
occurs(tpext, type2): bool

implement occurs(X0, tp1) =
case+ tp1 of 
| T2Pext(X1) => 
  if X0 = X1 then true
  else false
| T2Pfun(tp11, tp12) =>
  (occurs(X0, tp11) || occurs(X0, tp12))
| T2Ptup(tp11, tp12) => 
  (occurs(X0, tp11) || occurs(X0, tp12))
| _ => false


implement
unify(T1, T2) =
let
val T1 = eval(T1)
val T2 = eval(T2)
//
fun
auxvar
(X1: tpext, T2: type2): bool =
(
case+ T2 of
| T2Pext(X2) =>
  if X1 = X2
  then true
  else (X1.type(T2); true)
| _(*non-TPext*) =>
  if occurs(X1, T2)
  then false else (X1.type(T2); true)
)
//
in
case+
(T1, T2) of
|
(T2Pext(X1), _) => auxvar(X1, T2)
|
(_, T2Pext(X2)) => auxvar(X2, T1)
|
(T2Pbas(nm1), T2Pbas(nm2)) => nm1 = nm2
|
(T2Pfun(T11, T12), T2Pfun(T21, T22)) => 
 unify(T11, T21) && unify(T12, T22)
|
(T2Ptup(T11, T12), T2Ptup(T21, T22)) => 
 unify(T11, T21) && unify(T12, T22)
//
| (_, _) => false // unification failed
//
end


implement 
unify_if_present(T1, T2) =
case+ T2 of
| none() => true //unification passed, did not need to unify anything
| some(t0p1) => unify(T1, type_trans02(t0p1))


(* ****** ****** *)

(*
absimpl
t2erm_type =
$rec{
  t2erm_type= type2
, t2erm_node= t2erm_node
}
*)

val T2Pvoid = T2Pbas("void")
val T2Pint = T2Pbas("int")
val T2Pstring = T2Pbas("string")
val T2Pbool = T2Pbas("bool")

implement
trans12_term(t1m) =
(
case+ t1m of 
| T1Mnil() => $rec{t2erm_type=T2Pvoid, t2erm_node=T2Mnil()} 
| T1Mint(x) => $rec{t2erm_type=T2Pint, t2erm_node=T2Mint(x)} 
| T1Mbool(x) => $rec{t2erm_type=T2Pbool, t2erm_node=T2Mbool(x)} 
| T1Mstring(x) => $rec{t2erm_type=T2Pstring, t2erm_node=T2Mstring(x)} 
| T1Mvar(t1v1) => 
  let
    val-TMVAR(_, _, t2p1) = t1v1
  in
    $rec{t2erm_type=t2p1, t2erm_node=T2Mvar(t1v1)}
  end 
| T1Mlam(t1v, t0pop, t1m1) =>
  (
  let
    val-TMVAR(_, _, t2p1) = t1v
    val t2p1 = eval(t2p1)
  in
    if unify_if_present(t2p1, t0pop) 
    then 
    let 
      val t2m1 = trans12_term(t1m1)
    in
      $rec{
        t2erm_type=T2Pfun(t2p1, t2m1.t2erm_type), 
        t2erm_node=T2Mlam(t1v, t0pop, t2m1)}
    end
    else (fprint!(stderr_ref, "Lambda doesn't match type given"); $raise Illtyped2())
  end
  )
| T1Mapp(t1m1, t1m2) => 
  let
    val t2m1 = trans12_term(t1m1)
    val t2m2 = trans12_term(t1m2)
    val t2p1 = eval(t2m1.t2erm_type)
    val t2p2 = eval(t2m2.t2erm_type)
    val t2p1 = 
    (
    case+ t2p1 of
    | T2Pext(X) =>
      let
      val T = T2Pfun(T1, T2) in (X.type(T); T)
      end where
      {
        val T1 = type2_new()
        val T2 = type2_new()
      }
    | _ => t2p1
    ) : type2
  in 
    (
    case+ t2p1 of
    | T2Pfun(t2p11, t2p12) =>
      if unify(t2p11, t2p2)
      then $rec{
        t2erm_type = t2p12,
        t2erm_node = T2Mapp(t2m1, t2m2)
      } 
      else (fprint!(stderr_ref, "TMapp : arg type mismatch"); $raise Illtyped2())
    | _ => (fprint!(stderr_ref, "TMapp : Not a function type"); $raise Illtyped2())
    )
  end
| T1Mift(t1m1, t1m2, t1mop1) =>
  let 
    val t2m1 = trans12_term(t1m1)
    val t2m2 = trans12_term(t1m2)
    val t2p1 = t2m1.t2erm_type
    val t2p2 = t2m2.t2erm_type
    val t2mop1 = trans12_termopt(t1mop1)
    val t2p0 = 
    (
    case+ t2mop1 of
    | none() => t2p2
    | some(t2m3) => 
      if unify(t2p2, t2m3.t2erm_type)
      then t2p2 else (fprint!(stderr_ref, "Could not unify types of then and else"); $raise Illtyped2())
    )
  in
    if unify(t2p1, T2Pbool)
    then $rec{
      t2erm_type = t2p2,
      t2erm_node = T2Mift(t2m1, t2m2, t2mop1)
    }
    else (fprint!(stderr_ref, "If : value not boolean"); $raise Illtyped2())
  end
| T1Mopr(op1, t1ms) =>
  let
    val t2ms = trans12_termlst(t1ms)
    val ret_t2p = ret_type_opr(op1, t2ms)
  in 
    $rec{
      t2erm_type = ret_t2p,
      t2erm_node = T2Mopr(op1, t2ms)
    }
  end
| T1Mtup(t1m1, t1m2) =>
  let
    val t2m1 = trans12_term(t1m1)
    val t2m2 = trans12_term(t1m2)
  in
    $rec{
      t2erm_type = T2Ptup(t2m1.t2erm_type, t2m2.t2erm_type),
      t2erm_node = T2Mtup(t2m1, t2m2)
    }
  end
| T1Mann(t1m1, t0p1) =>
  let
    val t2m1 = trans12_term(t1m1)
    val t2p1 = type_trans02(t0p1)
  in
    if unify(t2p1, t2m1.t2erm_type)
    then t2m1
    else (fprint!(stderr_ref, "TMann : annotation and inferred types don't match"); $raise Illtyped2())
  end
| T1Mlet(t1ms, t1m1) => 
  let
    val t2ms = trans12_tdcllst(t1ms)
    val t2m1 = trans12_term(t1m1)
  in
    $rec{
      t2erm_type = t2m1.t2erm_type,
      t2erm_node = T2Mlet(t2ms, t2m1)
    }
  end 
| T1Mfix(t1vf, t1vx, t0pop1, t0pop2, t1m1) =>
  let
    val- TMVAR(_, _, t2pf) = t1vf  
    val- TMVAR(_, _, t2px) = t1vx
    val t2m1 = trans12_term(t1m1)
  in
    if (unify_if_present(t2px, t0pop1) 
      && unify_if_present(t2m1.t2erm_type, t0pop2)
      && unify(t2pf, T2Pfun(t2px, t2m1.t2erm_type)))
    then $rec{
      t2erm_type = t2pf,
      t2erm_node = T2Mfix(t1vf, t1vx, t0pop1, t0pop2, t2m1)
    }
    else (fprint!(stderr_ref, "TMfix : type error"); $raise Illtyped2())
  end
)

implement 
trans12_termopt(t1mop) =
case+ t1mop of
| none() => none()
| some(T) => some(trans12_term(T))

implement
trans12_termlst(t1ms) =
(
case+ t1ms of
| nil() => nil()
| cons(t1m, t1ms1) => cons(trans12_term(t1m), trans12_termlst(t1ms1))
)

implement 
ret_type_opr(op1, t2ms) =
case+ op1 of
| "fst" => 
  (
  case- t2ms of
  | t2m1::nil() => 
    (
    case+ eval(t2m1.t2erm_type) of
    | T2Pext(X) => 
      let
      val T = T2Ptup(T1, T2) in (X.type(T); T1)
      end where
      {
        val T1 = type2_new()
        val T2 = type2_new()
      }
    | T2Ptup(t2p1, t2p2) => t2p1
    | _ => (fprint!(stderr_ref, "fst applied on non tuple type"); $raise Illtyped2())
    )
  )
| "snd" =>
  (
  case- t2ms of
  | t2m1::nil() => 
    (
    case+ eval(t2m1.t2erm_type) of
    | T2Pext(X) => 
      let
      val T = T2Ptup(T1, T2) in (X.type(T); T2)
      end where
      {
        val T1 = type2_new()
        val T2 = type2_new()
      }
    | T2Ptup(t2p1, t2p2) => t2p2
    | _ => (fprint!(stderr_ref, "snd applied on non tuple type"); $raise Illtyped2())
    )
  )
| "print" => T2Pvoid //no need to check types 
| _ => 
  let
    val- some(CTP(t2ps, ret_t2p)) = oprnm_get_ctype2(op1)
  in
    if type_checklst(t2ps, t2ms)
    then ret_t2p
    else (fprint!(stderr_ref, "Opr type check failed"); $raise Illtyped2())
  end

// Will throw a case- matching error if lists of different lengths
implement
type_checklst(t2ps, t2ms) = 
case- (t2ps, t2ms) of
| (nil(), nil()) => true
| (t2p::t2ps1, t2m::t2ms1) => 
  if unify(t2p, t2m.t2erm_type)
  then type_checklst(t2ps1, t2ms1)
  else false

(* ****** ****** *)

implement
trans12_tdcl(t1d) =
let
  val T1DCL(t1v, t1m) = t1d
  val t2m = trans12_term(t1m)
  val- TMVAR(_, _, t2p0) = t1v
in
  if unify(t2p0, t2m.t2erm_type)
  then T2DCL(t1v, t2m)
  else $raise Illtyped2() //would never happen
end

implement trans12_tdcllst(t1ds) =
case+ t1ds of
| nil() => nil()
| t1d::t1ds1 => trans12_tdcl(t1d)::trans12_tdcllst(t1ds1)

(* ****** ****** *)

local

typedef
xcts =
mylist
($tup(oprnm, ctype2))
val
theCTmap =
ref<xcts>(mylist_nil())

in (*in-of-local*)

// Note : if an operator is not found in this list, 
// then it is assumed it can work with any type
// anything that works with more than 1 but not all types
// will be handled separately ("!=")
// "fst" and "snd" handled separately since their return type is not fixed
implement
oprnm_get_ctype2(x0) =
(
  loop(theCTmap[])
) where
{
fun
loop
(xcts: xcts): ctype2opt =
(
case+ xcts of
| mylist_nil() =>
  myoptn_none()
| mylist_cons(xct0, xcts) =>
  if
  x0 = xct0.0
  then myoptn_some(xct0.1) else loop(xcts)
)
} // oprnm_get_ctype2

implement
oprnm_set_ctype2(x0, ct) =
let
val xcts = theCTmap[]
in
  theCTmap[] := mylist_cons($tup(x0, ct), xcts)
end // oprnm_set_ctype2

end // end of [local]

val () =
oprnm_set_ctype2
("+", CTP(T2Pint :: T2Pint :: nil(), T2Pint))
val () =
oprnm_set_ctype2
("-", CTP(T2Pint :: T2Pint :: nil(), T2Pint))
val () =
oprnm_set_ctype2
("*", CTP(T2Pint :: T2Pint :: nil(), T2Pint))
val () =
oprnm_set_ctype2
("/", CTP(T2Pint :: T2Pint :: nil(), T2Pint))
val () =
oprnm_set_ctype2
("%", CTP(T2Pint :: T2Pint :: nil(), T2Pint))
val () =
oprnm_set_ctype2
("=", CTP(T2Pint :: T2Pint :: nil(), T2Pbool))
val () =
oprnm_set_ctype2
("<", CTP(T2Pint :: T2Pint :: nil(), T2Pbool))
val () =
oprnm_set_ctype2
(">", CTP(T2Pint :: T2Pint :: nil(), T2Pbool))
val () =
oprnm_set_ctype2
("<=", CTP(T2Pint :: T2Pint :: nil(), T2Pbool))
val () =
oprnm_set_ctype2
(">=", CTP(T2Pint :: T2Pint :: nil(), T2Pbool))


(* ****** ****** *)

implement
print_type2(tp) =
fprint_type2(stdout_ref, tp)

implement
fprint_type2(out, tp0) =
(
case+ tp0 of
| T2Pext(tv) =>
  (case+ tv.type() of
  | none() => fprint!(out, "T2Pext[", "None", "]") (* Meaning type not solved *)
  | some(T) => fprint!(out, "T2Pext[", T, "]")
  )
| T2Pbas(nm) =>
  fprint!(out, "T2Pbas(", nm, ")")
| T2Pfun(tp1, tp2) =>
  fprint!(out, "T2Pfun(", tp1, ", ", tp2, ")")
| T2Ptup(tp1, tp2) =>
  fprint!(out, "T2Ptup(", tp1, ", ", tp2, ")")
)

implement
fprint_t2erm(out, t2m) = 
fprint!(out, "T2Mterm{type=", t2m.t2erm_type, ";", "node=", t2m.t2erm_node, "}")

(*
implement
fprint_t2erm_onlytype(out, t2m) = 
fprint!(out, "T2Mterm{type=", t2m.t2erm_type, "}")
*)

implement
fprint_t2erm_node(out, x0) = 
(
case+ x0 of
| T2Mnil() =>
  fprint!(out, "T2Mnil()")
| T2Mint(i0) =>
  fprint!(out, "T2Mint(", i0, ")")
| T2Mbool(b0) =>
  fprint!(out, "T2Mbool(", b0, ")")
| T2Mstring(s0) =>
  fprint!(out, "T2Mstring(", s0, ")")
//
| T2Mvar(id) =>
  fprint!(out, "T2Mvar(", id, ")")
| T2Mapp(t0m1, t0m2) =>
  fprint!(out, "T2Mapp(", t0m1, "; ", t0m2, ")")
| T2Mlam
  (idx, targ, body) =>
  fprint!(out, "T2Mlam(", idx, "; ", targ, "; ", body, ")")
//
| T2Mift
  (t0m1, t0m2, opt3) =>
  fprint!(out, "T2Mift(", t0m1, "; ", t0m2, "; ", opt3, ")")
//
| T2Mopr(opnm, t0ms) =>
  fprint!(out, "T2Mopr(", opnm, "; ", t0ms, ")")
//
| T2Mtup(t0m1, t0m2) =>
  fprint!(out, "T2Mtup(", t0m1, "; ", t0m2, ")")
//
| T2Mann(t0m1, t0p2) =>
  fprint!(out, "T2Mann(", t0m1, "; ", t0p2, ")")
//
| T2Mlet(t0ds, t0m2) =>
  fprint!(out, "T2Mlet(", t0ds, "; ", t0m2, ")")
//
| T2Mfix
  (idf, idx, targ, tres, body) =>
  fprint!(out, "T2Mfix(", idf, "; ", idx, "; ", targ, "; ", tres, "; ", body, ")")
)

implement
fprint_t2dcl(out, x0) = 
(
case+ x0 of
| T2DCL(id1, t0m2) =>
  fprint!(out, "T2DCL(", id1, "; ", t0m2, ")")
)

(* ****** ****** *)
