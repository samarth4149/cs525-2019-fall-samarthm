(*
** For your
** final project
*)

(* ****** ****** *)

#staload
"./../../mylib/mylib.sats"

(* ****** ****** *)
//
typedef tpnam = string
//
(* ****** ****** *)
//
datatype
type0 =
//
| T0Pbas of tpnam // base
//
| T0Ptup of // tuples
  (type0(*fst*), type0(*snd*))
//
| T0Pfun of // functions
  (type0(*arg*), type0(*res*))
//
typedef type0opt = myoptn(type0)
typedef type0lst = mylist(type0)
//
(* ****** ****** *)
//
datatype
ctype0 =
  | CTP of (type0lst, type0)
//
(* ****** ****** *)
//
fun
print_type0: print_type(type0)
fun
fprint_type0: fprint_type(type0)
//
overload print with print_type0
overload fprint with fprint_type0
//
fun
print_ctype0: print_type(ctype0)
fun
fprint_ctype0: fprint_type(ctype0)
//
overload print with print_ctype0
overload fprint with fprint_ctype0
//
(* ****** ****** *)

typedef t0var = string
typedef oprnm = string

(* ****** ****** *)

datatype
t0dcl =
| T0DCL of
  (t0var, t0erm)
//
and t0erm =
//
  | T0Mnil of () // for voids
  | T0Mint of int // for integers
  | T0Mbtf of bool // for booleans
  | T0Mstr of string // for strings
//
  | T0Mvar of t0var
  | T0Mlam of
    (t0var, type0opt, t0erm)
  | T0Mapp of (t0erm, t0erm)
//
  | T0Mift of (t0erm, t0erm, t0ermopt)
//
  | T0Mopr of (oprnm, t0ermlst)
//
  | T0Mtup of (t0erm, t0erm)
//
  | T0Mann of (t0erm, type0) // type-annotation
//
  | T0Mlet of
    (t0dclist(*local*), t0erm(*scope*)) // local bindings
//
  | T0Mfix of
    ( t0var(*f*)
    , t0var(*x*)
    , type0opt(*arg*)
    , type0opt(*res*), t0erm) // Y(lam f.lam x.<body>)
//
where
t0dclist = mylist(t0dcl)
and
t0ermlst = mylist(t0erm)
and
t0ermopt = myoptn(t0erm)

(* ****** ****** *)

datatype p0grm =
| P0GRM of (t0dclist, t0erm)

(* ****** ****** *)
//
fun
print_t0erm: print_type(t0erm)
fun
fprint_t0erm: fprint_type(t0erm)
//
overload print with print_t0erm
overload fprint with fprint_t0erm
//
fun
print_t0dcl: print_type(t0dcl)
fun
fprint_t0dcl: fprint_type(t0dcl)
//
overload print with print_t0dcl
overload fprint with fprint_t0dcl
//
fun
print_p0grm: print_type(p0grm)
fun
fprint_p0grm: fprint_type(p0grm)
//
overload print with print_p0grm
overload fprint with fprint_p0grm
//
(* ****** ****** *)
//
// Resolving binding/scoping issues
//
(* ****** ****** *)

(* ****** ****** *)
//
abstype tpext_type = ptr
typedef tpext = tpext_type
//
(* ****** ****** *)

//
datatype
type2 =
//
| T2Pbas of tpnam // base
//
| T2Ptup of // tuples
  (type2(*fst*), type2(*snd*))
//
| T2Pfun of // functions
  (type2(*arg*), type2(*res*))
//
| T2Pext of tpext // existential

(* ****** ****** *)

fun
type_trans02: type0 -> type2

(* ****** ****** *)

abstype tmvar_type = ptr

datatype
tmvar =
TMVAR of
(t0var, int(*stamp*), type2)

(* ****** ****** *)

datatype
t1dcl =
| T1DCL of
  (tmvar, t1erm)
//
and t1erm =
//
  | T1Mnil of ()
  | T1Mint of int
  | T1Mbool of bool
(*
  | T1Mfloat of double
*)
  | T1Mstring of string
//
  | T1Mvar of tmvar
  | T1Mlam of
    (tmvar, type0opt, t1erm)
  | T1Mapp of (t1erm, t1erm)
//
  | T1Mift of (t1erm, t1erm, t1ermopt)
//
  | T1Mopr of (oprnm, t1ermlst)
//
  | T1Mtup of (t1erm, t1erm)
//
  | T1Mann of (t1erm, type0) // type-annotation
//
  | T1Mlet of
    (t1dclist(*local*), t1erm(*scope*)) // local bindings
//
  | T1Mfix of
    ( tmvar(*f*)
    , tmvar(*x*)
    , type0opt(*arg*)
    , type0opt(*res*), t1erm) // Y(lam f.lam x.<body>)
//
where
t1dclist = mylist(t1dcl)
and
t1ermlst = mylist(t1erm)
and
t1ermopt = myoptn(t1erm)

(* ****** ****** *)

datatype p1grm =
| P1GRM of (t1dclist, t1erm)

(* ****** ****** *)
//
fun
print_t1erm: print_type(t1erm)
fun
fprint_t1erm: fprint_type(t1erm)
//
overload print with print_t1erm
overload fprint with fprint_t1erm
//
fun
print_t1dcl: print_type(t1dcl)
fun
fprint_t1dcl: fprint_type(t1dcl)
//
overload print with print_t1dcl
overload fprint with fprint_t1dcl
//
fun
print_p1grm: print_type(p1grm)
fun
fprint_p1grm: fprint_type(p1grm)
//
overload print with print_p1grm
overload fprint with fprint_p1grm
//

fun
print_tmvar: print_type(tmvar)
fun
fprint_tmvar: fprint_type(tmvar)
//
overload print with print_tmvar
overload fprint with fprint_tmvar

fun
eq_tmvar_tmvar
(tmvar, tmvar): bool
overload = with eq_tmvar_tmvar

(* ****** ****** *)

fun trans01_term : t0erm -> t1erm
fun trans01_tdcl : t0dcl -> t1dcl
local
  typedef t0dclist = mylist(t0dcl)
  typedef t1dclist = mylist(t1dcl)
in
  fun trans01_tdcllst : t0dclist -> t1dclist
end
// NOTE : currently unused
fun trans01_pgrm : p0grm -> p1grm

(* ****** ****** *)
//
typedef type2opt = myoptn(type2)
typedef type2lst = mylist(type2)

(* ****** ****** *)


fun
eq_tpext_tpext
(tpext, tpext): bool
overload = with eq_tpext_tpext

fun
tpext_new(): tpext
and
type2_new(): type2


fun
tpext_get_type
(tpext): myoptn(type2)

fun
tpext_set_type
(X: tpext, sol: type2): void

overload .type with tpext_get_type
overload .type with tpext_set_type
//
(* ****** ****** *)
//
datatype
ctype2 =
  | CTP of (type2lst, type2)


typedef
ctype2opt = myoptn(ctype2)

fun
oprnm_get_ctype2(x0: oprnm): ctype2opt
fun
oprnm_set_ctype2(x0: oprnm, ct: ctype2): void
//
(* ****** ****** *)
//
fun
print_type2: print_type(type2)
fun
fprint_type2: fprint_type(type2)
//
overload print with print_type2
overload fprint with fprint_type2
//
fun
print_ctype2: print_type(ctype2)
fun
fprint_ctype2: fprint_type(ctype2)
//
overload print with print_ctype2
overload fprint with fprint_ctype2
//
(* ****** ****** *)

//abstype t2erm_type = ptr
//typedef t2erm = t2erm_type

(* ****** ****** *)

datatype
t2dcl =
| T2DCL of
  (tmvar, t2erm)
//
and t2erm_node =
//
  | T2Mnil of ()
  | T2Mint of int
  | T2Mbool of bool
(*
  | T2Mfloat of double
*)
  | T2Mstring of string
//
  | T2Mvar of tmvar
  | T2Mlam of
    (tmvar, type0opt, t2erm)
  | T2Mapp of (t2erm, t2erm)
//
  | T2Mift of (t2erm, t2erm, t2ermopt)
//
  | T2Mopr of (oprnm, t2ermlst)
//
  | T2Mtup of (t2erm, t2erm)
//
  | T2Mann of (t2erm, type0) // type-annotation
//
  | T2Mlet of
    (t2dclist(*local*), t2erm(*scope*)) // local bindings
//
  | T2Mfix of
    ( tmvar(*f*)
    , tmvar(*x*)
    , type0opt(*arg*)
    , type0opt(*res*), t2erm) // Y(lam f.lam x.<body>)
//
//  | T2Mcast of (t2erm, type0) // for internalization of type-errors
//
where
t2erm = 
$rec{
  t2erm_type= type2
, t2erm_node= t2erm_node
}
and
t2dclist = mylist(t2dcl)
and
t2ermlst = mylist(t2erm)
and
t2ermopt = myoptn(t2erm)


(* ****** ****** *)

fun
print_t2erm: print_type(t2erm)
fun
fprint_t2erm: fprint_type(t2erm)
//
overload print with print_t2erm
overload fprint with fprint_t2erm
//
fun
print_t2dcl: print_type(t2dcl)
fun
fprint_t2dcl: fprint_type(t2dcl)
//
overload print with print_t2dcl
overload fprint with fprint_t2dcl
//

fun
print_t2erm_node: print_type(t2erm_node)
fun
fprint_t2erm_node: fprint_type(t2erm_node)
//
overload print with print_t2erm_node
overload fprint with fprint_t2erm_node
//

(*
fun
print_t2erm_onlytype: print_type(t2erm_node)
fun
fprint_t2erm_onlytype: fprint_type(t2erm_node)
//
overload print with print_t2erm_onlytype
overload fprint with fprint_t2erm_onlytype
//
*)
(* ****** ****** *)

fun trans12_term : t1erm -> t2erm
fun trans12_termlst : t1ermlst -> t2ermlst
fun trans12_termopt : t1ermopt -> t2ermopt
fun trans12_tdcl : t1dcl -> t2dcl
local
  typedef t1dclist = mylist(t1dcl)
  typedef t2dclist = mylist(t2dcl)
in
  fun trans12_tdcllst : t1dclist -> t2dclist
end

fun ret_type_opr: (oprnm, t2ermlst) -> type2
fun type_checklst: (type2lst, t2ermlst) -> bool

(* ****** ****** *)

datatype p2grm =
| P2GRM of (t2dclist, t2erm)

(* ****** ****** *)

datatype
t3reg = T3REG of int(*stamp*)

(* ****** ****** *)

fun
t3reg_new(): t3reg

(* ****** ****** *)

datatype
t3val =
| T3Vnil of ()
| T3Vbool of int
| T3Vint of int
| T3Vstr of string
| T3Vfun of ()
| T3Vtmp of t3reg
| T3Varg of () 
| T3Venv of int
| T3Vclo of (int(*stamp*), t3vals(*local env*), t3inss(*body*), t3val(*res*))

and
t3ins =
| T3Imov of (t3reg, t3val)
| T3Itup of (t3reg, t3val, t3val)
| T3Ical of (t3reg, t3val, t3val)
| T3Iopr of (t3reg, oprnm, t3vals)
| T3Iift of (t3val, t3inss, t3inss)

where
t3vals = mylist(t3val)
and
t3inss = mylist(t3ins)
and
t3map = mylist($tup(tmvar, t3val))
and
t3env = 
$rec{
  local_env = t3map,
  arg_var = tmvar,
  ext_env = mylist(tmvar)
}


(* ****** ****** *)
(*
abstype
t3env_type = ptr
typedef
t3env = t3env_type
*)
fun
compile :
(t3env, t2erm) -> (t3inss, t3val)

fun
compile_decl :
(t3env, t2dcl) -> (t3inss, t3env) // returns modified environment

fun
compile_decllst :
(t3env, t2dclist) -> (t3inss, t3env) // returns modified environment

fun
compile_var :
(t3env, t2erm) -> (t3inss, t3val)
and
compile_lam :
(t3env, t2erm) -> (t3inss, t3val)
and
compile_fix :
(t3env, t2erm) -> (t3inss, t3val)

fun
compile_opr :
(t3env, t2erm) -> (t3inss, t3val)

(* ****** ****** *)

fun
eq_t3reg_t3reg
(t3reg, t3reg): bool
overload = with eq_t3reg_t3reg

fun
print_t3reg: print_type(t3reg)
fun
fprint_t3reg: fprint_type(t3reg)
//
overload print with print_t3reg
overload fprint with fprint_t3reg

fun
print_t3val: print_type(t3val)
fun
fprint_t3val: fprint_type(t3val)
//
overload print with print_t3val
overload fprint with fprint_t3val

fun
print_t3ins: print_type(t3ins)
fun
fprint_t3ins: fprint_type(t3ins)
//
overload print with print_t3ins
overload fprint with fprint_t3ins

(* ****** ****** *)

fun emit_tval : (FILEref, t3val) -> void
fun emit_tins : (FILEref, t3ins) -> void
fun emit_tinss : (FILEref, t3inss) -> void

(* ****** ****** *)

fun get_funs3 : (t3val, t3vals) -> t3vals
fun get_funs2 : (t3vals, t3vals) -> t3vals
fun get_funs1 : (t3inss, t3vals) -> t3vals
fun get_funs : t3inss -> t3vals
fun emit_funs : (FILEref, t3vals) -> void
fun emit_fun_decls : (FILEref, t3vals) -> void
fun emit_fun_defs : (FILEref, t3vals) -> void

// find and emit register declarations
fun get_regs : (t3inss, mylist(t3reg)) -> mylist(t3reg)
fun emit_regs_used : (FILEref, mylist(t3reg)) -> void

//fun emit_reg_decls : FILEref -> void

fun emit_tvals : (FILEref, t3vals) -> void
fun emit_t3opr : (FILEref, t3ins) -> void

(* ****** ****** *)
//
fun
project_main0
{n:int | n >= 1}(int(n), !argv(n)): void
//
(* ****** ****** *)

(* end of [project.sats] *)
