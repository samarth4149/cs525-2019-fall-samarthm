(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)
//
(* ****** ****** *)

#staload
"./../../mylib/mylib.sats"
#staload _ =
"./../../mylib/mylib.dats"

#staload
UN =
"prelude/SATS/unsafe.sats"

(* ****** ****** *)

#define nil mylist_nil
#define :: mylist_cons
#define cons mylist_cons

#define none myoptn_none
#define some myoptn_some

(* ****** ****** *)

#staload "./project.sats"

exception MainCall of ()
exception OpNotFound of ()


(* ****** ****** *)



local

val
theRegStamp = ref<int>(0)

in(*in-of-local*)

implement
t3reg_new() =
let
val n = theRegStamp[] in theRegStamp[] := n+1; T3REG(n)
end

end // end of [local]

implement 
eq_t3reg_t3reg(t3r1, t3r2) = 
let
  val T3REG(r1) = t3r1
  val T3REG(r2) = t3r2
in
  r1 = r2
end

val theCloStamp = ref<int>(0)

fun
new_clostamp(): int =
let
val n = theCloStamp[] in theCloStamp[] := n+1; n 
end


implement 
compile(env0, t2m) =
case- t2m.t2erm_node of
| T2Mnil() => 
  (nil(), T3Vnil())
| T2Mint(x) => 
  (nil(), T3Vint(x))
| T2Mbool(x) =>
  if x then (nil(), T3Vbool(1)) else (nil(), T3Vbool(0))  
| T2Mstring(x) =>
  (nil(), T3Vstr(x))
//
| T2Mvar(_) => 
  compile_var(env0, t2m)
| T2Mlam(_, _, _) =>
  compile_lam(env0, t2m)
| T2Mapp(t2m1, t2m2) =>
  let
    val res = t3reg_new()
    val (t3is1, t3v1) = compile(env0, t2m1)
    val (t3is2, t3v2) = compile(env0, t2m2)
  in
    //t3v1 should be a T3Vclo
    (mylist_extend(mylist_append(t3is1, t3is2), T3Ical(res, t3v1, t3v2)), T3Vtmp(res)) 
  end
//
| T2Mift(t2m1, t2m2, t2mop1) => 
  let
    val res = t3reg_new()
    val (t3is1, t3v1) = compile(env0, t2m1)
    val (t3is2, t3v2) = compile(env0, t2m2)
    val (t3is3, t3v3) =
    (
    case+ t2mop1 of
    | none() => (nil(), T3Vnil())
    | some(t2m3) => compile(env0, t2m3)
    ) : (t3inss, t3val) 
  in
    (mylist_extend(
      t3is1, 
      T3Iift(t3v1, 
             mylist_extend(t3is2, T3Imov(res, t3v2)), 
             mylist_extend(t3is3, T3Imov(res, t3v3)))
    ), T3Vtmp(res))
  end
//
| T2Mopr(_, _) => 
  compile_opr(env0, t2m)
//
| T2Mtup(t2m1, t2m2) =>
  let
    val res = t3reg_new()
    val (t3is1, t3v1) = compile(env0, t2m1)
    val (t3is2, t3v2) = compile(env0, t2m2)
  in
    (mylist_extend(mylist_append(t3is1, t3is2), T3Itup(res, t3v1, t3v2)), T3Vtmp(res))
  end
//
| T2Mlet(t2ds, t2m1) => 
  let
    val (t3is1, env1) = compile_decllst(env0, t2ds)
    val (t3is2, t3v1) = compile(env1, t2m1)
  in
    (mylist_append(t3is1, t3is2), t3v1)
  end
//
| T2Mfix(_, _, _, _, _) =>
  compile_fix(env0, t2m)


// returns index in the env; -1 if not present
fun find_in_local_env(env0: t3map, t1v0: tmvar) : int =
loop(env0, t1v0, 0)
where
{
fun loop(env0: t3map, t1v0: tmvar, idx: int): int =
case+ env0 of
| nil() => ~1
| e1::env1 => 
  if e1.0 = t1v0 then idx
  else loop(env1, t1v0, idx+1)
}

fun find_in_ext_env(env0: mylist(tmvar), t1v0: tmvar) : int =
loop(env0, t1v0, 0)
where
{
fun loop(env0: mylist(tmvar), t1v0: tmvar, idx: int): int =
case+ env0 of
| nil() => ~1
| e1::env1 => 
  if e1 = t1v0 then idx
  else loop(env1, t1v0, idx+1)
}

implement
compile_var(env0, t2m) =
let
  val- T2Mvar(t1v0) = t2m.t2erm_node
  val id_local = find_in_local_env(env0.local_env, t1v0)
in
  if id_local >= 0 
  then 
    let
      val t3mp = mylist_get_at(env0.local_env, id_local)
    in
      (nil(), t3mp.1)
    end
  else
    if t1v0 = env0.arg_var then (nil(), T3Varg())
    else
    let
      val id_ext = find_in_ext_env(env0.ext_env, t1v0)  
    in
      //has to be present in ext_env
      (nil(), T3Venv(id_ext))
    end
end


implement
compile_lam(env0, t2m) =
let
  val- T2Mlam(t1v0, t0pop0, t2m1) = t2m.t2erm_node
  val local_env = env0.local_env
  val arg_var = env0.arg_var
  val ext_env = env0.ext_env
  val ext_env = arg_var::ext_env
  val ext_env = mylist_append(mylist_map(local_env, lam(x) => get_first(x)), ext_env) 
  val env1 = $rec{
    local_env = nil(),
    arg_var = t1v0,
    ext_env = ext_env
  }
  val (t3is1, t3v1) = compile(env1, t2m1)
in
  (nil(), T3Vclo(new_clostamp(), 
                 mylist_map(local_env, lam(x) => get_second(x)), 
                 t3is1, t3v1))
end
where
{
  fun get_first(mp : $tup(tmvar, t3val)) : tmvar = mp.0
  fun get_second(mp : $tup(tmvar, t3val)) : t3val = mp.1
}

implement
compile_fix(env0, t2m) =
let
  val- T2Mfix(t1vf, t1vx, _, _, t2m1) = t2m.t2erm_node
  val local_env = env0.local_env
  val arg_var = env0.arg_var
  val ext_env = env0.ext_env
  val ext_env = arg_var::ext_env
  val ext_env = mylist_append(mylist_map(local_env, lam(x) => get_first(x)), ext_env)
  val fmap = $tup(t1vf, T3Vfun())  
  val env1 = $rec{
    local_env = fmap::nil(),
    arg_var = t1vx,
    ext_env = ext_env
  }
  val (t3is1, t3v1) = compile(env1, t2m1)
in
  (nil(), T3Vclo(new_clostamp(), 
                 mylist_map(local_env, lam(x) => get_second(x)), 
                 t3is1, t3v1))
end
where
{
  fun get_first(mp : $tup(tmvar, t3val)) : tmvar = mp.0
  fun get_second(mp : $tup(tmvar, t3val)) : t3val = mp.1
}


fun 
compile_list(env0:t3env, t2ms: t2ermlst) : (t3inss, t3vals) =
case+ t2ms of
| nil() => (nil(), nil())
| t2m1::t2ms1 => 
  let
    val (t3i1, t3v1) = compile(env0, t2m1)
    val (t3is1, t3vs1) = compile_list(env0, t2ms1)
  in
    (mylist_append(t3i1, t3is1), t3v1::t3vs1)
  end

implement 
compile_opr(env0, t2m) =
let
  val res = t3reg_new()
  val-T2Mopr(op1, t2ms) = t2m.t2erm_node
  val (t3is, t3vs) = compile_list(env0, t2ms)
in
  (mylist_extend(t3is, T3Iopr(res, op1, t3vs)), T3Vtmp(res))
end

(* ****** ****** *)

implement 
compile_decl(env0, t2d) =
let
  val T2DCL(t1v1, t2m1) = t2d
  val res = t3reg_new()
  val (t3is, t3v1) = compile(env0, t2m1)
  val env1 = $rec{
    local_env = $tup(t1v1, T3Vtmp(res))::env0.local_env,
    arg_var = env0.arg_var,
    ext_env = env0.ext_env
  }
in
  (mylist_extend(t3is, T3Imov(res, t3v1)), env1)
end

implement 
compile_decllst(env0, t2ds) = 
case+ t2ds of
| nil() => (nil(), env0)
| t2d1::t2ds1 =>
  let
    val (t3is1, env1) = compile_decl(env0, t2d1)
    val (t3is2, env2) = compile_decllst(env1, t2ds1)
  in
    (mylist_append(t3is1, t3is2), env2)
  end

(* ****** ****** *)

implement
fprint_val<t3reg> = fprint_t3reg

implement
fprint_val<t3val> = fprint_t3val

implement
fprint_val<t3ins> = fprint_t3ins

implement 
print_t3reg(x) = fprint_t3reg(stdout_ref, x)

implement 
print_t3val(x) = fprint_t3val(stdout_ref, x)

implement 
print_t3ins(x) = fprint_t3ins(stdout_ref, x)

implement
fprint_t3reg(out, t3r) =
case+ t3r of
| T3REG(x) => fprint!(out, "T3REG(", x, ")")

implement
fprint_t3val(out, t3v) = 
case+ t3v of
| T3Vnil() => 
  fprint!(out, "T3Vnil()")
| T3Vbool(x) => 
  fprint!(out, "T3Vbool(", x, ")")
| T3Vint(x) =>
  fprint!(out, "T3Vint(", x, ")")
| T3Vstr(x) =>
  fprint!(out, "T3Vstr(", x, ")")
| T3Vfun() =>
  fprint!(out, "T3Vfun()")
| T3Vtmp(t3r) =>
  fprint!(out, "T3Vtmp(", t3r, ")")
| T3Varg() =>
  fprint!(out, "T3Varg()") 
| T3Venv(idx) =>
  fprint!(out, "T3Venv(", idx, ")")
| T3Vclo(f_id, t3vs, t3is, t3v1) =>
  fprint!(out, "T3Vclo(id=", f_id, ", env=(", t3vs, "), body=(", t3is, "), ret=(", t3v1, ") )")

implement
fprint_t3ins(out, t3i) = 
case+ t3i of
| T3Imov(t3r, t3v) =>
  fprint!(out, "T3Imov(", t3r, ", ", t3v, ")")
| T3Itup(t3r, t3v1, t3v2) =>
  fprint!(out, "T3Itup(", t3r, ", ", t3v1, ", ", t3v2, ")")
| T3Ical(t3r, t3v1, t3v2) =>
  fprint!(out, "T3Ical(", t3r, ", ", t3v1, ", ", t3v2, ")")
| T3Iopr(t3r, op1, t3vs) =>
  fprint!(out, "T3Iopr(", t3r, ", OP(", op1, "), vals=(", t3vs, "))")
| T3Iift(t3v, t3is1, t3is2) =>
  fprint!(out, "T3Iift(IF(", t3v, "), THEN(", t3is1, "), ELSE(", t3is2, ") )")

(* ****** ****** *)

// Go through list of instructions and find all closures, 
// since they need to become functions. Need not create the closure
// environment right there. That has to be done when encountering the 
// closure among instructions

// make a note of all functions which are closures 
// in a list of instructions. Each closure needs a stamp
// Each function would be named : "F"+str(stamp)


// get the list of closures from a single value (go into its instructions 
// if its a closure)
implement 
get_funs3(t3v: t3val, clos: t3vals) : t3vals = 
case+ t3v of
| T3Vclo(_, _, t3is1, t3v1) => 
  mylist_append(get_funs1(t3is1, get_funs3(t3v1, nil())), t3v::clos)
| _ => clos

//get the list of closures from a list of values
implement 
get_funs2(t3vs: t3vals, clos: t3vals) : t3vals = 
case+ t3vs of
| nil() => clos
| t3v1::t3vs1 => mylist_append(get_funs3(t3v1, nil()), get_funs2(t3vs1, clos))

// will return a list of all closures found
implement 
get_funs1(t3is : t3inss, clos : t3vals) : t3vals = 
case+ t3is of
| nil() => clos
| t3i1::t3is1 => 
  let
    val clos1 = 
    (
    case+ t3i1 of
    | T3Imov(_, t3v1) => 
      get_funs3(t3v1, clos)
    | T3Itup(_, t3v1, t3v2) =>
      mylist_append(get_funs3(t3v1, nil()), get_funs3(t3v2, clos))
    | T3Ical(_, t3v1, t3v2) =>
      mylist_append(get_funs3(t3v1, nil()), get_funs3(t3v2, clos))
    | T3Iopr(_, _, t3vs1) =>
      get_funs2(t3vs1, clos)
    | T3Iift(_, t3is1, t3is2) =>
      mylist_append(get_funs1(t3is1, nil()), get_funs1(t3is2, clos))
    ): t3vals
  in
    get_funs1(t3is1, clos1)
  end

implement
get_funs(t3is) =
get_funs1(t3is, nil())

implement
emit_funs(out, t3vs) =
(emit_fun_decls(out, t3vs); emit_fun_defs(out, t3vs))

implement
emit_fun_decls(out, t3vs) = 
case+ t3vs of
| nil() => ()
| t3v1::t3vs1 => 
  let
    val- T3Vclo(t3v1_id, _, _, _) = t3v1
  in
    (fprintln!(out, "lamval F", t3v1_id, "(lamval arg, lamval *env, int env_len);"); 
     emit_fun_decls(out, t3vs1))
  end

fun
reg_in_list(t3r1 : t3reg, t3rs : mylist(t3reg)) : bool = 
case+ t3rs of
| nil() => false
| t3r2::t3rs1 => 
  if t3r1 = t3r2 then true
  else reg_in_list(t3r1, t3rs1)

implement 
get_regs(t3is : t3inss, regs : mylist(t3reg)) : mylist(t3reg) =
case+ t3is of
| nil() => regs 
| t3i1::t3is1 =>
  (
  case+ t3i1 of
  | T3Imov(t3r1, _) => 
    if reg_in_list(t3r1, regs) then get_regs(t3is1, regs)
    else get_regs(t3is1, t3r1::regs)
  | T3Itup(t3r1, _, _) => 
    if reg_in_list(t3r1, regs) then get_regs(t3is1, regs)
    else get_regs(t3is1, t3r1::regs)
  | T3Ical(t3r1, _, _) => 
    if reg_in_list(t3r1, regs) then get_regs(t3is1, regs)
    else get_regs(t3is1, t3r1::regs)
  | T3Iopr(t3r1, _, _) => 
    if reg_in_list(t3r1, regs) then get_regs(t3is1, regs)
    else get_regs(t3is1, t3r1::regs)
  | T3Iift(_, t3is1, t3is2) =>
    get_regs(t3is1, get_regs(t3is2, regs)) 
  )

implement
emit_regs_used(out : FILEref, t3rs : mylist(t3reg)) : void = 
case+ t3rs of
| nil() => ()
| t3r1::nil() => 
  let
    val T3REG(x) = t3r1
  in
    fprintln!(out, "R", x, ";")
  end
| t3r1::t3rs1 => 
  let
    val T3REG(x) = t3r1
  in
    (fprint!(out, "R", x, ", "); emit_regs_used(out, t3rs1))
  end

implement
emit_fun_defs(out, t3vs) = 
case+ t3vs of
| nil() => ()
| t3v1::t3vs1 =>
  let
    val- T3Vclo(t3v1_id, _, t3is1, t3v2) = t3v1
    val regs_used = get_regs(t3is1, nil())
  in
    (
    fprintln!(out, "lamval F", t3v1_id, "(lamval arg, lamval *env, int env_len){");
    // find and emit reg declarations for regs in the following instructions
    if mylist_length(regs_used) > 0 
    then (fprint!(out, "lamval "); emit_regs_used(out, regs_used));
    emit_tinss(out, t3is1);
    fprint!(out, "return ");
    emit_tval(out, t3v2);
    fprint!(out, ";");
    fprintln!(out, "}");
    emit_fun_defs(out, t3vs1)
    )
  end

(* ****** ****** *)

// When a list of instructions is encountered, find all registers 
// since they need to be declared as lamvals. Do not go into the  
// instructions inside a closure

// in emit_tinss, declare registers first

fun 
copy_tvals_to_env(out : FILEref, t3vs : t3vals, idx:int) =
case+ t3vs of
| nil() => ()
| t3v1::t3vs1 => 
  (fprint!(out, "clo_env[", idx, "] = "); 
   emit_tval(out, t3v1);
   fprint!(out, ","); 
   copy_tvals_to_env(out, t3vs1, idx+1)) 

implement 
emit_tval(out, t3v) = 
case+ t3v of
| T3Vnil() => ()
| T3Vbool(x) => fprint!(out, "T3Vbool(", x, ")")
| T3Vint(x) => fprint!(out, "T3Vint(", x, ")")
| T3Vstr(x) => fprint!(out, "T3Vstr(\"", x, "\")")
| T3Vfun() => 
  fprint!(out, "T3Vclo(env_len, env)")
| T3Vtmp(t3reg) => 
  let 
    val T3REG(rnum0) = t3reg
  in
    fprint!(out, "R", rnum0) 
  end
| T3Varg() => 
  fprint!(out, "arg")
| T3Venv(x) =>
  // first position in the closure is the function ptr
  fprint!(out, "env[", x + 1, "]") 
| T3Vclo(f_id, t3vs, t3is1, t3v1) => 
  let
    val t3vs_len = mylist_length(t3vs)
  in
    (
    fprint!(out, "(clo_env = malloc((", 
              1 + t3vs_len + 1 , " + env_len)*sizeof(lamval)),"); 
    fprint!(out, "clo_env[", 0, "] = &F", f_id,",");
    copy_tvals_to_env(out, t3vs, 1);
    // no such thing as arg in the outermost level, created a dummy
    fprint!(out, "clo_env[", 1+t3vs_len,"] = arg,");
    fprint!(out, "loop_copy(clo_env, env, ", 2 + t3vs_len, ", 1, env_len),");
    fprint!(out, "T3Vclo(env_len+", 1+t3vs_len,", clo_env))")
    )
  end

implement
emit_tvals(out, t3vs) = 
case+ t3vs of
| nil() => ()
| t3v1::nil() =>
  emit_tval(out, t3v1) 
| t3v1::t3vs1 => 
  (emit_tval(out, t3v1); fprint!(out, ", "); emit_tvals(out, t3vs1))

implement 
emit_tins(out, t3i) =
case+ t3i of
| T3Imov(t3r1, t3v1) =>
  let
    val T3REG(reg_num1) = t3r1
  in
    (fprint!(out, "R", reg_num1, " = "); emit_tval(out, t3v1); fprintln!(out, ";"))
  end
| T3Itup(t3r1, t3v1, t3v2) =>
  let 
    val T3REG(reg_num1) = t3r1
  in
    (
    fprint!(out, "R", reg_num1, " = "); 
    fprint!(out, "T3Vtup(");
    emit_tval(out, t3v1);
    fprint!(out, ", ");
    emit_tval(out, t3v2);
    fprintln!(out, ");")
    )
  end
| T3Ical(t3r1, t3v1, t3v2) =>
  let
    val T3REG(reg_num1) = t3r1
  in
    (
    fprint!(out, "R", reg_num1, " = ");
    fprint!(out, "FUNCLO_APP(");
    emit_tval(out, t3v1);
    fprint!(out, ", ");
    emit_tval(out, t3v2);
    fprintln!(out, ");")
    )
  end 
| T3Iift(t3v1, t3is1, t3is2) =>
  (
  fprint!(out, "if(get_bool(");
  emit_tval(out, t3v1); 
  fprintln!(out, ")){");
  emit_tinss(out, t3is1);
  fprintln!(out, "} else {");
  emit_tinss(out, t3is2);
  fprintln!(out, "}")
  )
| T3Iopr(_, _, _) =>
  emit_t3opr(out, t3i)

fun 
emit_print(out: FILEref, t3vs: t3vals) : void = 
case+ t3vs of
| nil() => ()
| t3v1::t3vs1 => 
  (fprint!(out, "T3Vopr_print("); emit_tval(out, t3v1); fprint!(out, ");");
   emit_print(out, t3vs1))

implement
emit_t3opr(out, t3i) =
let
  val- T3Iopr(t3r1, op1, t3vs) = t3i
  val T3REG(reg_num1) = t3r1 
in
  (
  fprint!(out, "R", reg_num1, " = ");
  (
  case+ op1 of
  | "+" => 
    (fprint!(out, "T3Vopr_add("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "-" => 
    (fprint!(out, "T3Vopr_sub("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "*" => 
    (fprint!(out, "T3Vopr_mul("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "/" => 
    (fprint!(out, "T3Vopr_div("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "%" => 
    (fprint!(out, "T3Vopr_mod("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | ">" => 
    (fprint!(out, "T3Vopr_gt("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "<" => 
    (fprint!(out, "T3Vopr_lt("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "=" => 
    (fprint!(out, "T3Vopr_eq("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "<=" => 
    (fprint!(out, "T3Vopr_lte("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | ">=" => 
    (fprint!(out, "T3Vopr_gte("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "!=" => 
    (fprint!(out, "T3Vopr_neq("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "fst" => 
    (fprint!(out, "T3Vopr_fst("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "snd" => 
    (fprint!(out, "T3Vopr_snd("); emit_tvals(out, t3vs); fprint!(out, ")"))
  | "print" => 
    emit_print(out, t3vs)
  | _ => $raise OpNotFound() // won't happen
  );
  fprintln!(out, ";")
  )
end

implement
emit_tinss(out, t3is) = 
case+ t3is of
| nil() => ()
| t3i1::t3is1 => 
  (emit_tins(out, t3i1); emit_tinss(out, t3is1))