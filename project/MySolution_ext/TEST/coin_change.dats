val theCoins = (1, (5, (10, 25)))

fun coin_get(n) =
  if n = 0 then fst(theCoins)
  else if n = 1 then fst(snd(theCoins))
  else if n = 2 then fst(snd(snd(theCoins)))
  else if n = 3 then snd(snd(snd(theCoins)))
  else (0 - 1) (* erroneous value *)
// end of [coin_get]

fun coin_change(sum: int): int =
let
  fun aux(sn) =
  let 
    val sum = fst(sn)
    val n   = snd(sn)
  in
    (
    //print("sum:", sum, ", n=", n, "\n");
    if sum > 0 then
      (if n >= 0 then ((*print(sum - coin_get(n), "\n");*) aux(sum, n-1) + aux(sum-coin_get(n), n)) else 0)
    else ((*print("here2")*); if sum < 0 then 0 else 1)
    )
  end
  // end of [aux]
in
  aux(sum, 3)
end // end of [coin_change]

fun main() = 
print("coin_change(100) = ", coin_change(100), "\n")