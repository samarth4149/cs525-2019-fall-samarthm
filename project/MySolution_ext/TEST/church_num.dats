val O = lam f => lam x => x
val S = lam n => lam f => lam x => f (n f x)

val plus = lam m => lam n => lam f => lam x => m f (n f x)

val C3 = S(S(S(O)))
val C2 = S(S(O))

val inc = lam x => (x + 1)
val num = (plus C3 C2) inc 0

fun main() =
print("church number 3 + 2 = ", num, "\n")