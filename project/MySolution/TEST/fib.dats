fun fib(nc) =
let
  val n = fst(nc)
  val c = snd(nc)
in
  if n = 0 then c(0)
  else if n = 1 then c(1)
  else fib(n-1, lam r1 => fib(n-2, lam r2 => c(r1+r2)))
end


fun main() =
print("fib = ", fib(10, lam x => x), "\n")