(* ****** ****** *)

val x = 1
(*val y = f(x+1)*)
val f = lam x => x + 1
val f = fix f(x:int): int => f(x+1)

val _ = fst((1, "2"))
val _ = let val x = 1 in x + x end

(*val _ = print(x1)*)

val x = (1,2): (int,int)

val f =
(lam x => x > 0) : int -> bool

fun f(x) =
if x > 0 then x * f(x-1) else 1

(* ****** ****** *)

fun
main() = print("[main] is here!")

(* ****** ****** *)

(* end of [test01.dats] *)
