(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)
//
(* ****** ****** *)

#staload
"./../../mylib/mylib.sats"
#staload _ =
"./../../mylib/mylib.dats"

(* ****** ****** *)

#define nil mylist_nil
#define :: mylist_cons
#define cons mylist_cons

(* ****** ****** *)

#staload "./project.sats"

(* ****** ****** *)

extern
fun
tmvar_new0(): tmvar
extern
fun
tmvar_new1(t0var): tmvar

(* ****** ****** *)

local 

typedef t0t1list = mylist($tup(t0var, tmvar))
typedef t1dclenv = $tup(t1dcl, t0t1list)
typedef t0ermlst = mylist(t0erm)
typedef t1ermlst = mylist(t1erm)
typedef t0dclist = mylist(t0dcl)
typedef t1dclist = mylist(t1dcl)
typedef t1dclistenv = $tup(t1dclist, t0t1list)

in 

extern
fun
trans01_term1(t0erm, t0t1list) : t1erm

extern
fun
trans01_termopt(t0ermopt, t0t1list) : t1ermopt

extern
fun
trans01_termlst(t0ermlst, t0t1list) : t1ermlst

extern
fun
find_tmvar(t0var, t0t1list) : tmvar

extern
fun
trans01_tdcl1(t0dcl, t0t1list(*env*)): t1dclenv

extern
fun
trans01_tdcllst1(t0dclist, t0t1list(*env*)): t1dclistenv

end //end of local
(* ****** ****** *) 

exception Binding01 of ()

(* ****** ****** *)

absimpl tmvar_type = tmvar

val theStamp = ref<int>(0)

fun
theStamp_get(): int =
let
val n = theStamp[] in theStamp[] := n+1; n 
end

implement
tmvar_new1(id) =
TMVAR(id, stamp, t2p) where
{
  val stamp = theStamp_get()
  val t2p = type2_new()
}

implement
eq_tmvar_tmvar(x, y) =
let
val TMVAR(_, x, _) = x
val TMVAR(_, y, _) = y in x = y
end

(* ****** ****** *)


implement
fprint_val<type0> = fprint_type0
implement
print_t1erm(x0) =
fprint_t1erm(stdout_ref, x0)
implement
print_t1dcl(x0) =
fprint_t1dcl(stdout_ref, x0)
implement
print_tmvar(x0) =
fprint_tmvar(stdout_ref, x0)

(* ****** ****** *)

implement
fprint_val<t1erm> = fprint_t1erm
implement
fprint_val<t1dcl> = fprint_t1dcl
implement
fprint_val<tmvar> = fprint_tmvar

(* ****** ****** *)

implement
fprint_t1erm(out, x0) = 
(
case+ x0 of
| T1Mnil() =>
  fprint!(out, "T1Mnil()")
| T1Mint(i0) =>
  fprint!(out, "T1Mint(", i0, ")")
| T1Mbool(b0) =>
  fprint!(out, "T1Mbool(", b0, ")")
| T1Mstring(s0) =>
  fprint!(out, "T1Mstring(", s0, ")")
//
| T1Mvar(id) =>
  fprint!(out, "T1Mvar(", id, ")")
| T1Mapp(t0m1, t0m2) =>
  fprint!(out, "T1Mapp(", t0m1, "; ", t0m2, ")")
| T1Mlam
  (idx, targ, body) =>
  fprint!(out, "T1Mlam(", idx, "; ", targ, "; ", body, ")")
//
| T1Mift
  (t0m1, t0m2, opt3) =>
  fprint!(out, "T1Mift(", t0m1, "; ", t0m2, "; ", opt3, ")")
//
| T1Mopr(opnm, t0ms) =>
  fprint!(out, "T1Mopr(", opnm, "; ", t0ms, ")")
//
| T1Mtup(t0m1, t0m2) =>
  fprint!(out, "T1Mtup(", t0m1, "; ", t0m2, ")")
//
| T1Mann(t0m1, t0p2) =>
  fprint!(out, "T1Mann(", t0m1, "; ", t0p2, ")")
//
| T1Mlet(t0ds, t0m2) =>
  fprint!(out, "T1Mlet(", t0ds, "; ", t0m2, ")")
//
| T1Mfix
  (idf, idx, targ, tres, body) =>
  fprint!(out, "T1Mfix(", idf, "; ", idx, "; ", targ, "; ", tres, "; ", body, ")")
)

implement
fprint_t1dcl(out, x0) = 
(
case+ x0 of
| T1DCL(id1, t0m2) =>
  fprint!(out, "T1DCL(", id1, "; ", t0m2, ")")
)

implement
fprint_tmvar(out, x0) = 
(
case+ x0 of
| TMVAR(t0v, st, _) =>
  fprint!(out, "TMVAR(", t0v, "; stamp=", st, ")")
)

(* ****** ****** *)

// TODO: initial env may not be empty but have things from previous 'let's
// in a t0decl list first env would be empty, but then it should get 
// carried over
// So this function may not be used
implement trans01_term(t0m0) =
trans01_term1(t0m0, mylist_nil()) 

implement
trans01_term1(t0m0, env0) = 
(
case+ t0m0 of
| T0Mnil() => T1Mnil()
| T0Mint(x1) => T1Mint(x1)
| T0Mbtf(x1) => T1Mbool(x1)
| T0Mstr(x1) => T1Mstring(x1)
| T0Mvar(t0v1) => T1Mvar(find_tmvar(t0v1, env0))
| T0Mlam(t0v1, t0p, t0m1) => 
  let 
    val t1v1 = tmvar_new1(t0v1)
  in
    T1Mlam(t1v1, t0p, trans01_term1(t0m1, mylist_cons($tup(t0v1, t1v1), env0)))
  end
| T0Mapp(t0m1, t0m2) => 
  let
    val t1m1 = trans01_term1(t0m1, env0)
    val t1m2 = trans01_term1(t0m2, env0)
  in 
    T1Mapp(t1m1, t1m2)
  end
| T0Mift(t0m1, t0m2, t0mop1) =>
  T1Mift(trans01_term1(t0m1, env0), trans01_term1(t0m2, env0), trans01_termopt(t0mop1, env0))
| T0Mopr(op0, t0ms) =>
  T1Mopr(op0, trans01_termlst(t0ms, env0))
//
| T0Mtup(t0m1, t0m2) =>
  T1Mtup(trans01_term1(t0m1, env0), trans01_term1(t0m2, env0))
//
| T0Mann(t0m1, tp0) => 
  T1Mann(trans01_term1(t0m1, env0), tp0)
//
| T0Mlet(t0ds, t0m1) =>
  let
    val ret = trans01_tdcllst1(t0ds, env0)
    val t1ds = ret.0
    val env1 = ret.1
  in
    T1Mlet(t1ds, trans01_term1(t0m1, env1))
  end
//
| T0Mfix(f, x, t0pop1, t0pop2, t0m1) =>
  let
    val f1 = tmvar_new1(f)
    val x1 = tmvar_new1(x)
    val env1 = $tup(f, f1)::$tup(x, x1)::env0
  in
    T1Mfix(f1, x1, t0pop1, t0pop2, trans01_term1(t0m1, env1))
  end
)


implement 
find_tmvar(t0v0, mps) =
(
case+ mps of 
| mylist_nil() => $raise Binding01()
| mylist_cons(mp0, mps1) => 
  if t0v0 = mp0.0 then mp0.1 
  else find_tmvar(t0v0, mps1)
)

implement
trans01_termopt(t0mop0, env0) =
(
case+ t0mop0 of
| myoptn_none() => myoptn_none()
| myoptn_some(t0m) => myoptn_some(trans01_term1(t0m, env0))
)

implement 
trans01_termlst(t0ms, env0) =
(
case+ t0ms of
| nil() => nil()
| cons(t0m1, t0ms1) => cons(trans01_term1(t0m1, env0), trans01_termlst(t0ms1, env0))
)

implement
trans01_tdcl1(t0d0, env0) = 
(
let
  val-T0DCL(t0v, t0m) = t0d0
  val t1m = trans01_term1(t0m, env0)
  val t1v = tmvar_new1(t0v)
  val t1d = T1DCL(t1v, t1m)
  val env1 = cons($tup(t0v, t1v), env0) 
in
  $tup(t1d, env1)
end
)

implement
trans01_tdcllst1(t0ds, env0) = 
(
case+ t0ds of
| nil() => $tup(nil(), env0)
| cons(t0d1, t0ds1) => 
  let
    val t1denv = trans01_tdcl1(t0d1, env0)
    val t1d1 = t1denv.0
    val env1 = t1denv.1
    val ret = trans01_tdcllst1(t0ds1, env1)
    val t1ds = ret.0
    val ret_env = ret.1
  in
    $tup(cons(t1d1, t1ds), ret_env)
  end
)

implement 
trans01_tdcl(t0d0) =
let
  val ret = trans01_tdcl1(t0d0, nil())
in
  ret.0
end

implement
trans01_tdcllst(t0ds) =
let
  val ret = trans01_tdcllst1(t0ds, nil())
in
  ret.0
end
(* ****** ****** *)