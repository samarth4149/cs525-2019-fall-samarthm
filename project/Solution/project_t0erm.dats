(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)
//
(* ****** ****** *)

#staload
"./../../mylib/mylib.sats"
#staload _ =
"./../../mylib/mylib.dats"

(* ****** ****** *)

#staload "./project.sats"

(* ****** ****** *)

implement
print_type0(x0) =
fprint_type0(stdout_ref, x0)
implement
print_t0erm(x0) =
fprint_t0erm(stdout_ref, x0)
implement
print_t0dcl(x0) =
fprint_t0dcl(stdout_ref, x0)

(* ****** ****** *)

implement
fprint_val<type0> = fprint_type0
implement
fprint_val<t0erm> = fprint_t0erm
implement
fprint_val<t0dcl> = fprint_t0dcl

(* ****** ****** *)

implement
fprint_type0
(out, x0) =
(
case+ x0 of
| T0Pbas(nam) =>
  fprint!(out, "T0Pbas(", nam, ")")
| T0Ptup
  (t0p1, t0p2) =>
  fprint!(out, "T0Ptup(", t0p1, "; ", t0p2, ")")
| T0Pfun
  (t0p1, t0p2) =>
  fprint!(out, "T0Pfun(", t0p1, "; ", t0p2, ")")
)

(* ****** ****** *)

implement
fprint_t0erm
(out, x0) =
(
case+ x0 of
| T0Mnil() =>
  fprint!(out, "T0Mnil(", ")")
| T0Mint(i0) =>
  fprint!(out, "T0Mint(", i0, ")")
| T0Mbtf(b0) =>
  fprint!(out, "T0Mbtf(", b0, ")")
| T0Mstr(s0) =>
  fprint!(out, "T0Mstr(", s0, ")")
//
| T0Mvar(id) =>
  fprint!(out, "T0Mvar(", id, ")")
| T0Mapp(t0m1, t0m2) =>
  fprint!(out, "T0Mapp(", t0m1, "; ", t0m2, ")")
| T0Mlam
  (idx, targ, body) =>
  fprint!(out, "T0Mlam(", idx, "; ", targ, "; ", body, ")")
//
| T0Mift
  (t0m1, t0m2, opt3) =>
  fprint!(out, "T0Mift(", t0m1, "; ", t0m2, "; ", opt3, ")")
//
| T0Mopr(opnm, t0ms) =>
  fprint!(out, "T0Mopr(", opnm, "; ", t0ms, ")")
//
| T0Mtup(t0m1, t0m2) =>
  fprint!(out, "T0Mtup(", t0m1, "; ", t0m2, ")")
| T0Mfst(t0m1) => fprint!(out, "T0Mfst(", t0m1, ")")
| T0Msnd(t0m1) => fprint!(out, "T0Msnd(", t0m1, ")")
//
| T0Mann(t0m1, t0p2) =>
  fprint!(out, "T0Mann(", t0m1, "; ", t0p2, ")")
//
| T0Mlet(t0ds, t0m2) =>
  fprint!(out, "T0Mlet(", t0ds, "; ", t0m2, ")")
//
| T0Mfix
  (idf, idx, targ, tres, body) =>
  fprint!(out, "T0Mfix(", idf, "; ", idx, "; ", targ, "; ", tres, "; ", body, ")")
)

(* ****** ****** *)

implement
fprint_t0dcl(out, x0) =
(
case+ x0 of
| T0DCL(id1, t0m2) =>
  fprint!(out, "T0DCL(", id1, "; ", t0m2, ")")
)

(* ****** ****** *)

(* end of [project_t0erm.dats] *)
