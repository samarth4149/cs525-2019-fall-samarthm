(* ****** ****** *)
//
#staload
ERR =
"./Xanadu/srcgen/xats/SATS/xerrory.sats"
//
(* ****** ****** *)
//
#staload
STM =
"./Xanadu/srcgen/xats/SATS/stamp0.sats"
#staload
SYM =
"./Xanadu/srcgen/xats/SATS/symbol.sats"
//
(* ****** ****** *)
//
#staload
FP0 =
"./Xanadu/srcgen/xats/SATS/filpath.sats"
//
  typedef
  fpath_t = $FP0.filpath
  macdef
  dirbase =
  $FP0.filpath_dirbase
  macdef
  fpath_make = $FP0.filpath_make
//
(* ****** ****** *)
//
#staload
GLO =
"./Xanadu/srcgen/xats/SATS/global.sats"
#staload
FS0 =
"./Xanadu/srcgen/xats/SATS/filsrch.sats"
//
(* ****** ****** *)
//
#staload
"./Xanadu/srcgen/xats/SATS/basics.sats"
//
#staload
"./Xanadu/srcgen/xats/SATS/lexing.sats"
//
#staload
"./Xanadu/srcgen/xats/SATS/parsing.sats"
//
#staload
"./Xanadu/srcgen/xats/SATS/synread.sats"
//
#staload
"./Xanadu/srcgen/xats/SATS/staexp1.sats"
#staload
"./Xanadu/srcgen/xats/SATS/dynexp1.sats"
//
#staload
"./Xanadu/srcgen/xats/SATS/trans01.sats"
#staload
"./Xanadu/srcgen/xats/SATS/t1xread.sats"
//
(* ****** ****** *)
//
#staload
_(*TMP*) =
"./Xanadu/srcgen/xats/DATS/staexp0_print.dats"
#staload
_(*TMP*) =
"./Xanadu/srcgen/xats/DATS/dynexp0_print.dats"
//
#staload
_(*TMP*) =
"./Xanadu/srcgen/xats/DATS/staexp1_print.dats"
#staload
_(*TMP*) =
"./Xanadu/srcgen/xats/DATS/dynexp1_print.dats"
//
(* ****** ****** *)

(* end of [libxatsopt.hats] *)

