(* ****** ****** *)

datatype
mylist(a:t0ype) =
| mylist_nil of ()
| mylist_cons of (a, mylist(a))

(* ****** ****** *)

fun
{a:t0ype}
mylist_length(xs: mylist(a)): int

fun
{a:t0ype}
mylist_rev(xs: mylist(a)): mylist(a)

fun
{a1:t0ype}{a2:t0ype}
mylist_map(xs: mylist(a1), f: a1-<cloref1>a2): mylist(a2) 
(* ****** ****** *)
//
fun
{a:t0ype}
print_mylist: mylist(a) -> void
fun
{a:t0ype}
prerr_mylist: mylist(a) -> void
//
fun
{a:t0ype}
fprint_mylist
(out: FILEref, xs: mylist(a)): void
fun{}
fprint_mylist$sep(out: FILEref): void
//
overload print with print_mylist
overload prerr with prerr_mylist
overload fprint with fprint_mylist
//
(* ****** ****** *)

(* end of [mylib.sats] *)
