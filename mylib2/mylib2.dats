(* ****** ****** *)

#staload "./mylib2.sats"

(* ****** ****** *)

#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)

(* ****** ****** *)

implement
{a}
mylist_length
  (xs) =
( loop(xs, 0) ) where
{
//
fun
loop
(xs: mylist(a), n: int) =
case+ xs of
| mylist_nil() => n
| mylist_cons(_, xs) => loop(xs, n+1)
//
} (* end of [mylist_length] *)

(* ****** ****** *)

implement
{a}
mylist_rev(xs: mylist(a)): mylist(a) = 
let
	fun helper (ys: mylist(a), acc:mylist(a)) : mylist(a) =
	(case+ ys of
	| mylist_nil() => acc
	| mylist_cons(y1, y1s) => helper(y1s, mylist_cons(y1, acc))
	)
in
	helper(xs, mylist_nil())
end

(* ****** ****** *)

implement
{a1}{a2}
mylist_map(xs, f) = 
let
	fun helper(ys:mylist(a1), acc:mylist(a2)) =
	(case+ ys of
	| mylist_nil() => acc
	| mylist_cons(y1, y1s) => helper(y1s, mylist_cons(f(y1), acc))
	)
in
	helper(mylist_rev(xs), mylist_nil())
end

(* ****** ****** *)
//
implement
{a}(*tmp*)
print_mylist(xs) =
fprint_mylist(stdout_ref, xs)
implement
{a}(*tmp*)
prerr_mylist(xs) =
fprint_mylist(stderr_ref, xs)
//
implement
{a}(*tmp*)
fprint_mylist
  (out, xs) =
( loop(xs, 0) ) where
{
fun
loop
(xs: mylist(a), i: int): void =
(
case+ xs of
| mylist_nil() => ()
| mylist_cons(x0, xs) =>
  (
  if
  i > 0
  then
  fprint_mylist$sep<>(out);
  fprint_val<a>(out, x0); loop(xs, i+1)
  )
)
}
//
implement{}
fprint_mylist$sep(out) = fprint_string(out, ", ")
//
(* ****** ****** *)

(* end of [mylib.dats] *)
